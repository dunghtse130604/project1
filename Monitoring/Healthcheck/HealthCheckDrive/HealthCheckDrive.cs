﻿using JB2.Core;
using Microsoft.Azure.Devices.Client;
using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Data.Common;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ExecutionContext = Microsoft.Azure.WebJobs.ExecutionContext;
using TransportType = Microsoft.Azure.Devices.Client.TransportType;

namespace JB2.Healthcheck
{
    public class HealthCheckDrive
    {
        public static async Task<HttpResponseMessage> Run(HttpRequestMessage req, CloudBlobContainer healthCheckContainer,
            ILogger logger, ExecutionContext context)
        {
            var deviceId = ExternalConfig.HealthCheckDeviceIdDrive;
            using (var deviceClient = DeviceClient.CreateFromConnectionString(HealthCheckUtil.GetDeviceConnectionString(deviceId), TransportType.Mqtt))
            {
                await deviceClient.OpenAsync();
                var engineOnTime = UnixTime.Now;
                var time = ((engineOnTime + 59) / 60) * 60;

                //Prepare drive csv data with gzip
                var oneMinuteDriveData = File.ReadAllLines(context.FunctionDirectory + @"\data\drive.csv");
                ConvertDriveData(oneMinuteDriveData, deviceId, engineOnTime, time);
                var dataBytes =
                    GzipUtils.Compress(Encoding.UTF8.GetBytes(string.Join("\n", oneMinuteDriveData)));

                //Prepare drive json content
                var driveContent = new
                {
                    engineOnTime,
                    time,
                    duration = oneMinuteDriveData.Length - 1
                };

                //Send drive message to Gateway
                var d2cMsg = HealthCheckUtil.CreateMessage(MessageType.Drive, driveContent, dataBytes);
                await deviceClient.SendEventAsync(d2cMsg);
                await Task.Delay(TimeSpan.FromSeconds(5));

                //Wait drive message reached to SCOM EventHub
                if (!await healthCheckContainer.WaitMessageReached(deviceId, MessageType.Drive, time))
                {
                    logger.Error("Timeout due to drive message not reached");
                    return new HttpResponseMessage(HttpStatusCode.RequestTimeout);
                }
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
        }

        private static void ConvertDriveData(string[] driveData, string deviceId, long engineOnTime, long time)
        {
            for (var i = 1; i < driveData.Length; i++)
            {
                var cells = driveData[i].Split('\t');
                cells[0] = deviceId;
                cells[2] = engineOnTime.ToString();
                cells[3] = time.ToString();
                time++;
                driveData[i] = string.Join("\t", cells);
            }
        }
    }
}