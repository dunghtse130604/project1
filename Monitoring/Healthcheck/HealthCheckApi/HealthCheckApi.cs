﻿using JB2.Core;
using Microsoft.Extensions.Logging;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace JB2.Healthcheck
{
    public class HealthCheckApi
    {
        static HealthCheckApi() { new VersionLogger(); }

        public static async Task<HttpResponseMessage> Run(HttpRequestMessage req, ILogger logger)
        {
            var accessKey = AppEnvironment.RestApiAccessKey1;
            var apiUrl = HealthCheckUtil.GetRestApi(ExternalConfig.HealthCheckDeviceIdApi);

            using (var client = new HttpClient())
            {
                var validReq = new HttpRequestMessage(HttpMethod.Get, apiUrl + "/valid");
                await SecurityUtils.AddAuthenticationAsync(validReq, accessKey);
                var res = await client.SendAsync(validReq);
                if (res.StatusCode != HttpStatusCode.OK)
                {
                    return res;
                }

                var fcwReq = new HttpRequestMessage(HttpMethod.Get, apiUrl + "/fcw");
                await SecurityUtils.AddAuthenticationAsync(fcwReq, accessKey);
                res = await client.SendAsync(fcwReq);
                if (res.StatusCode != HttpStatusCode.OK)
                {
                    return res;
                }
            }
            return new HttpResponseMessage(HttpStatusCode.OK);
        }
    }
}