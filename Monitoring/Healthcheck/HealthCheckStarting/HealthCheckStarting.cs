﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using JB2.Core;
using Microsoft.Azure.Devices.Client;
using Microsoft.Extensions.Logging;
using TransportType = Microsoft.Azure.Devices.Client.TransportType;

namespace JB2.Healthcheck
{
    public class HealthCheckStarting
    {
        public static async Task<HttpResponseMessage> Run(HttpRequestMessage req, ILogger logger)
        {
            var connStr = HealthCheckUtil.GetDeviceConnectionString(ExternalConfig.HealthCheckDeviceIdStartup);

            // Get URL API in deviceTwin and create url to call API.
            using (var deviceClient = DeviceClient.CreateFromConnectionString(connStr, TransportType.Mqtt))
            {
                await deviceClient.OpenAsync();
                var twin = await deviceClient.GetTwinAsync();
                var desiredProps = twin.Properties.Desired;
                if (desiredProps.Contains("url"))
                {
                    var configurationApi = $"{desiredProps["url"]}&type=initialize";
                    using (var httpClient = new HttpClient())
                    {
                        return await httpClient.SendAsync(new HttpRequestMessage(HttpMethod.Post, configurationApi));
                    }
                }
            }
            logger.Error("Device twin cannot found: 'url' property.");
            return req.CreateResponse(HttpStatusCode.InternalServerError);
        }
    }
}