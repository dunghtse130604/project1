﻿using JB2.Core;
using Microsoft.Azure.Devices.Client;
using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ExecutionContext = Microsoft.Azure.WebJobs.ExecutionContext;
using TransportType = Microsoft.Azure.Devices.Client.TransportType;

namespace JB2.Healthcheck
{
    public class HealthCheckEvent
    {
        public static async Task<HttpResponseMessage> Run(HttpRequestMessage req, CloudBlobContainer healthCheckContainer,
            ILogger logger, ExecutionContext context)
        {
            var deviceId = ExternalConfig.HealthCheckDeviceIdEvent;
            using (var deviceClient = DeviceClient.CreateFromConnectionString(HealthCheckUtil.GetDeviceConnectionString(deviceId), TransportType.Mqtt))
            {
                await deviceClient.OpenAsync();
                var time = UnixTime.Now;

                //Register OnMdtRequest invoking from the server and get the payload
                var mdtRequest = new StringBuilder();
                await deviceClient.SetMethodHandlerAsync("OnMdtRequest", UploadMdtTask, mdtRequest);

                //Prepare event json content
                var eventContent = new
                {
                    time,
                    gps = new Gps
                    {
                        Valid = 1,
                        Latitude = 35.785018,
                        Longitude = 139.313097,
                        Speed = 32,
                        Heading = 193
                    },
                    flags = new Flags
                    {
                        Shock = 1,
                        Accel = 1
                    },
                    strength = 0
                };

                //Send event message to Gateway
                var eventMsg = HealthCheckUtil.CreateMessage(MessageType.Event, eventContent);
                await deviceClient.SendEventAsync(eventMsg);

                //Wait event message reached to SCOM EventHub
                if (!await healthCheckContainer.WaitMessageReached(deviceId, MessageType.Event, time))
                {
                    logger.Error("Timeout due to event message not reached");
                    return new HttpResponseMessage(HttpStatusCode.RequestTimeout);
                }

                //Wait OnMdtRequest invoked
                int timeout;
                for (timeout = 90; timeout > 0; timeout--)
                {
                    await Task.Delay(TimeSpan.FromSeconds(1));
                    if (mdtRequest.Length > 0) break;
                }
                if (timeout == 0)
                {
                    logger.Error("Timeout due to OnMdtRequest not invoked");
                    return new HttpResponseMessage(HttpStatusCode.RequestTimeout);
                }

                // Send Mdt messages
                var oneSecSensorData = File.ReadAllLines(context.FunctionDirectory + @"\data\sensor.csv");
                var h264Bin = File.ReadAllBytes(context.FunctionDirectory + @"\data\h264");

                foreach (var reqTimeStr in mdtRequest.ToString().Split(','))
                {
                    //Prepare sensor csv data with gzip as data bytes
                    var reqTime = long.Parse(reqTimeStr);
                    ConvertSensorData(oneSecSensorData, deviceId, reqTime);
                    var dataBytes = GzipUtils.Compress(Encoding.UTF8.GetBytes(string.Join("\n", oneSecSensorData)));

                    //Prepare mdt json content
                    var mdtContent = new Mdt
                    {
                        Time = reqTime,
                        Video = new Video
                        {
                            Width = 1280,
                            Height = 720,
                            Fps = 10,
                            Bitrate = 1024
                        }
                    };

                    //Send mdt message to Gateway
                    var mdtMsg = HealthCheckUtil.CreateMessage(MessageType.Mdt, mdtContent, dataBytes, h264Bin);
                    await deviceClient.SendEventAsync(mdtMsg);
                }
                await Task.Delay(TimeSpan.FromSeconds(1));

                //Wait eventData message reached to SCOM EventHub
                if (!await healthCheckContainer.WaitMessageReached(deviceId, MessageType.EventData, time))
                {
                    logger.Error("Timeout due to eventData message not reached");
                    return new HttpResponseMessage(HttpStatusCode.RequestTimeout);
                }

                return new HttpResponseMessage(HttpStatusCode.OK);
            }
        }

        private static Task<MethodResponse> UploadMdtTask(MethodRequest methodRequest, object context)
        {
            var payload = JsonConvert.DeserializeObject<dynamic>(methodRequest.DataAsJson);
            var mdtRequest = context as StringBuilder;
            mdtRequest.Clear();
            mdtRequest.Append(payload.mdtRequest.Value);
            return Task.FromResult(new MethodResponse(Encoding.UTF8.GetBytes("{}"), 200));
        }

        private static void ConvertSensorData(string[] sensorData, string deviceId, long beginTime)
        {
            var msecTime = beginTime * 1000;
            for (var i = 1; i < sensorData.Length; i++)
            {
                var cells = sensorData[i].Split('\t');
                cells[0] = deviceId;
                cells[2] = msecTime.ToString();
                msecTime += 10;
                sensorData[i] = string.Join("\t", cells);
            }
        }
    }
}