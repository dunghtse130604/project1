﻿using JB2.Core;
using Microsoft.Azure.Devices.Client;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using ExecutionContext = Microsoft.Azure.WebJobs.ExecutionContext;

namespace JB2.Healthcheck
{
    public class ScomHealthcheckDrive
    {
        private const string DeviceConnectionString = "DRIVE_devices.csv";

        private const string DriveDataCsv = "drive.csv";

        private static string[] _rawOneMinuteData;

        public static async Task Run(TimerInfo driveTimer, ILogger logger, ExecutionContext context)
        {
            var pathConnString = Path.Combine(context.FunctionDirectory, DeviceConnectionString);
            var connStrings = File.ReadAllLines(pathConnString);

            var pathDriveData = Path.Combine(context.FunctionDirectory, DriveDataCsv);
            _rawOneMinuteData = File.ReadAllLines(pathDriveData);

            //Replace engnineOnTime in file by realtime: (DateTimeOff.Now / 60 ) * 60 (Round the engineOnTime)
            var engineOnTime = (UnixTime.Now / 60) * 60;

            var allTask = new List<Task>();
            foreach (var connString in connStrings)
            {
                allTask.Add(SendDriveMessageTask(connString, _rawOneMinuteData, engineOnTime, logger));
            }
            await Task.WhenAll(allTask);
            logger.Debug($"Send drive messages at time: {engineOnTime}");
        }

        private static async Task SendDriveMessageTask(string connectionString, string[] rawOneMinuteData, long engineOnTime, ILogger logger)
        {
            var deviceId = GetDeviceId(connectionString);
            DeviceClient deviceClient = null;
            try
            {
                deviceClient = DeviceClient.CreateFromConnectionString(connectionString, TransportType.Mqtt);
                await deviceClient.OpenAsync();

                //Generate 1 min drive data and compress it to gzip
                var convertedData = ConvertDriveData(rawOneMinuteData, deviceId, engineOnTime);
                var dataBytes = GzipUtils.Compress(Encoding.UTF8.GetBytes(convertedData));
                var content = new
                {
                    engineOnTime,
                    time = engineOnTime,
                    duration = 60
                };

                //Generate device2Cloud message
                var message = HealthCheckUtil.CreateMessage(MessageType.Drive, content, dataBytes);
                await deviceClient.SendEventAsync(message);
            }
            catch (Exception e)
            {
                logger.Exception(e);
            }
            finally
            {
                if (deviceClient != null) await deviceClient.CloseAsync();
            }
        }

        public static string GetDeviceId(string connectionString)
        {
            var buider = new DbConnectionStringBuilder
            {
                ConnectionString = connectionString
            };
            return buider["DeviceId"].ToString();
        }

        private static string ConvertDriveData(string[] driveData, string deviceId, long engineOnTime)
        {
            var convertedData = new StringBuilder();
            convertedData.AppendLine(driveData[0]);
            for (var i = 1; i < driveData.Length; i++)
            {
                //Replace time of firstLine = engineOnTime, the next time = previous time + 1
                var convertedRecord = ConvertRecord(driveData[i], deviceId, engineOnTime, engineOnTime + i - 1);
                //if is end file Append
                if (i == driveData.Length - 1)
                {
                    convertedData.Append(convertedRecord);
                }
                else
                {
                    convertedData.AppendLine(convertedRecord);
                }
            }
            return convertedData.ToString();
        }

        private static string ConvertRecord(string line, string deviceId, long engineOnTime, long time)
        {
            var cells = line.Split('\t');
            cells[0] = deviceId;
            cells[2] = engineOnTime.ToString();
            cells[3] = time.ToString();
            return string.Join("\t", cells);
        }
    }
}