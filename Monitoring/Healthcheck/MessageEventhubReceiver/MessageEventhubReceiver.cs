﻿using JB2.Core;
using Microsoft.Extensions.Logging;
using Microsoft.ServiceBus.Messaging;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Threading.Tasks;

namespace JB2.Healthcheck
{
    public class MessageEventhubReceiver
    {
        public static async Task Run(CloudBlobContainer healthCheckContainer, EventData[] eventHubMsg, ILogger logger)
        {
            foreach (var eventData in eventHubMsg)
            {
                var msg = new JB2Message(eventData);
                if (!msg.ParseData(logger))
                {
                    continue;
                }
                logger = logger.AddDeviceIdProp(msg.DeviceId);
                if (msg.DeviceId == ExternalConfig.HealthCheckDeviceIdEvent || msg.DeviceId == ExternalConfig.HealthCheckDeviceIdReset)
                {
                    if (msg.MessageType == MessageType.Event || msg.MessageType == MessageType.EventData || msg.MessageType == MessageType.Reset)
                    {
                        var blob = healthCheckContainer.GetBlockBlobReference($"{msg.MessageType}_{msg.Properties[EventDataProps.DataId]}");
                        await blob.UploadTextAsync("");
                    }
                }

                //TEMPORAL: Logging SCOM healthcheck data
                if (msg.DeviceId.StartsWith("TCSTEST"))
                {
                    logger.Info("SCOM healthcheck event data");
                }

                //TEMPORAL: Check receive delay
                var delayTime = DateTime.UtcNow - eventData.EnqueuedTimeUtc;
                if (delayTime > TimeSpan.FromMinutes(5))
                {
                    logger.Warning(
                        $"Receive delay: DelayTime={delayTime}, EnqueueTime={eventData.EnqueuedTimeUtc}, DataId={msg.Properties[EventDataProps.DataId]}");
                }
            }
        }
    }
}