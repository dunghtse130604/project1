﻿using JB2.Core;
using Microsoft.Azure.Devices.Client;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using ExecutionContext = Microsoft.Azure.WebJobs.ExecutionContext;

namespace JB2.Healthcheck
{
    public class ScomHealthcheckEvent
    {
        private const string DeviceConnectionString = "EVENT_devices.csv";

        public static async Task Run(TimerInfo eventTimer, ExecutionContext context, ILogger logger)
        {
            var pathConnString = Path.Combine(context.FunctionDirectory, DeviceConnectionString);
            var connStrings = File.ReadAllLines(pathConnString);
            var allTasks = new List<Task>();
            var currentTime = UnixTime.Now;

            //Set real time to raw accel event 
            var eventData = new Event
            {
                Time = UnixTime.Now,
                Gps = new Gps
                {
                    Valid = 1,
                    Latitude = 35.785018,
                    Longitude = 139.313097,
                    Speed = 40,
                    Heading = 1
                },
                Flags = new Flags(),
            };

            foreach (var connection in connStrings)
            {
                var eventTask = SendNormalEventMsgTask(connection, eventData, logger);
                allTasks.Add(eventTask);
            }
            await Task.WhenAll(allTasks);
            logger.Debug($"Send event messages at time: {currentTime}");
        }

        private static async Task SendNormalEventMsgTask(string connection, Event eventData, ILogger logger)
        {
            DeviceClient deviceClient = null;
            try
            {
                deviceClient = DeviceClient.CreateFromConnectionString(connection, TransportType.Mqtt);

                //Open connect to server and set direct method handler
                await deviceClient.OpenAsync();
                var normalEventMessage = HealthCheckUtil.CreateMessage(MessageType.Event, eventData);
                await deviceClient.SendEventAsync(normalEventMessage);
            }
            catch (Exception e)
            {
                logger.Exception(e);
            }
            finally
            {
                if (deviceClient != null) await deviceClient.CloseAsync();
            }
        }
    }
}