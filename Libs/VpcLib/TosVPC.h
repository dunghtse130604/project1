/*
* TosVPC.h
*
*  Created on: Dec 6, 2016
*      Author: chiendh
*/

#ifndef TOSVPC_H_
#define TOSVPC_H_

#include <map>

#ifdef TOSVPC_API
#define EXTERN __declspec(dllexport)
#else
#define EXTERN __declspec(dllimport)
#endif

// Version of VPC library
#define VPC_VERSION "1.0.1"

namespace TosVPC {
    typedef public EXTERN struct _VPC_INIT_PARAM_ {
        int vp_y_samples;
        int vp_thresh_hold;
        int count_thresh_hold;
        int block_size_be;
        int block_slide_size;
        int be_thresh_hold;
        int be_y_samples;
    } VPC_INIT_PARAM;

    /**
    *   VP_Data is structure which store value of calculated vanishing point and bonnet edge value
    *   of a drive recorder
    */
#pragma pack(push, 1)
    typedef public EXTERN struct VP_Data {
        int VP_X;
        int VP_Y;
        int BE_P;
        int HD_VP_X;
        int HD_VP_Y;
        int HD_BE_P;
    } VP_Data;
#pragma pack(pop)

    /**
    *   Return code of wrapper library:
    *       TVPC_SUCCESSFUL: functions is executed successfully
    *       TVPC_FAILED: application passed invalid parameter into wrapper library: input null image data, null output object pointer
    *       TVPC_FAILED_ALLOCATE_MEMORY: there�s not enough memory to allocate during processing
    *       TVPC_WRONG_WIDTH_HEIGHT: application pass odd width or odd height or negative value of size of image into wrapper library
    *       TVPC_NEED_MORE_DATA: library cannot calculate result from current input image and notify that it need more input image to calculate
    *       TVPC_FILE_ERROR: there�s a problem when read/write file.
    *       TVPC_DRID_INVALID: application pass an invalid driver id (not 12 character string)
    *       TVPC_OBJECT_NULL: application call a wrapper API with a null TVPC object
    *       TVPC_NOT_INITIALIZED: application doesn�t call TVPC_Initialize API before calling other APIs
    */
    public enum EXTERN TVPC_STATUS {
        TVPC_SUCCESSFUL = 0,
        TVPC_FAILED = -1,
        TVPC_FAILED_ALLOCATE_MEMORY = -2,
        TVPC_WRONG_WIDTH_HEIGHT = -3,
        TVPC_NEED_MORE_DATA = -4,
        TVPC_FILE_ERROR = -5,
        TVPC_DRID_INVALID = -6,
        TVPC_OBJECT_NULL = -7,   // TVPC object null when pass as parameter
        TVPC_NOT_INITIALIZED = -8,
        TVPC_FAILED_CREATE_OBJECT = -9,
        TVPC_WRONG_DATA_COUNT = -10
    };

    class TVPC {
    private:
        // Used to check that the Initialize function has been called or not
        bool initialized;
        // USed to store settings that application set with Initialize function
        VPC_INIT_PARAM init_params;
    public:
        // Constructor
        EXTERN TVPC();

        // Initialize wrapper library with settings parameters
        EXTERN TVPC_STATUS TVPC_InitializeWithSetting(VPC_INIT_PARAM init_params);

        // Input image and process to calculate vanishing point and bonet edge
        // If there is not enough image to calculate, return TVPC_NEED_MORE_DATA
        // If successfully, return TVPC_SUCCESSFUL
        EXTERN TVPC_STATUS TVPC_SetImage2(VP_Data *vp_data_list, int data_count, unsigned char*  in_image,
            int  image_width, int  image_height, VP_Data*  vp_data, VP_Data*  vp_data_fixed);

    };
}

#endif /* TOSVPC_H_ */
