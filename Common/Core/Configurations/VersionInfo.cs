﻿using Microsoft.ApplicationInsights;
using Microsoft.ApplicationInsights.DataContracts;
using System;
using System.Collections.Generic;

namespace JB2.Core
{
    // Create this object in static constructor of one of Function classes in each AppService
    public class VersionLogger
    {
        // Do not add value for this constant.
        // Version string will be set by CICD not by manually.
        public static readonly string VersionString = "";
        
        public VersionLogger()
        {
            var appName = Environment.GetEnvironmentVariable("WEBSITE_SITE_NAME");
            var properties = new Dictionary<string, string>
            {
                { "AppName", appName },
                { "Version", VersionString }
            };
            new TelemetryClient()
                .TrackTrace($"AppName:{appName}, Version:{VersionString}", SeverityLevel.Warning, properties);
        }
    }
}