﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;

namespace JB2.Core
{
    public class ExternalConfig
    {
        public class ConfigJson
        {
            public int SEND_INTERVAL { get; set; }
            public int SEND_TIMEOUT { get; set; }
            public int PRE_TIME { get; set; }
            public int POST_TIME { get; set; }
            public string KITTING_SDID { get; set; }
            public int VPC_INTERVAL { get; set; }
            public int VPC_SPEED { get; set; }
            public int VPC_MAXNUM { get; set; }
            public int[] IGNORED_LOGS { get; set; }
            public int DOCUMENT_TTL { get; set; }
            public string HEALTHCHECK_DEVICE_ID_STARTUP { get; set; }
            public string HEALTHCHECK_DEVICE_ID_API { get; set; }
            public string HEALTHCHECK_DEVICE_ID_RESET { get; set; }
            public string HEALTHCHECK_DEVICE_ID_EVENT { get; set; }
            public string HEALTHCHECK_DEVICE_ID_DRIVE { get; set; }
            public int CONNECTIONS_LIMIT { get; set; }
            public int DAY_LIMIT { get; set; }
            public string[] DEBUG_SECTIONS { get; set; }
        }

        private static readonly Lazy<ConfigJson> instance = new Lazy<ConfigJson>(() =>
        {
            var externalConfigPath = @"D:\home\site\wwwroot\external.json";
#if DEBUG
            //externalConfigPath = "external.json";
#endif
            var config = JsonConvert.DeserializeObject<ConfigJson>(File.ReadAllText(externalConfigPath));
            if (config.CONNECTIONS_LIMIT > 0)
            {
                ServicePointManager.DefaultConnectionLimit = config.CONNECTIONS_LIMIT;
            }
            return config;
        });

        public static int SendInterval => instance.Value.SEND_INTERVAL;
        public static int SendTimeout => instance.Value.SEND_TIMEOUT;
        public static int PreTime => instance.Value.PRE_TIME;
        public static int PostTime => instance.Value.POST_TIME;
        public static string KittingSdId => instance.Value.KITTING_SDID;
        public static int VpcInterval => instance.Value.VPC_INTERVAL;
        public static int VpcSpeed => instance.Value.VPC_SPEED;
        public static int VpcMaxNum => instance.Value.VPC_MAXNUM;
        public static int DayLimit => instance.Value.DAY_LIMIT;
        public static int[] IgnoredLogs => instance.Value.IGNORED_LOGS;
        public static int DocumentTimeToLive => instance.Value.DOCUMENT_TTL * 24 * 60 * 60;
        public static string HealthCheckDeviceIdStartup => instance.Value.HEALTHCHECK_DEVICE_ID_STARTUP;
        public static string HealthCheckDeviceIdApi => instance.Value.HEALTHCHECK_DEVICE_ID_API;
        public static string HealthCheckDeviceIdReset => instance.Value.HEALTHCHECK_DEVICE_ID_RESET;
        public static string HealthCheckDeviceIdEvent => instance.Value.HEALTHCHECK_DEVICE_ID_EVENT;
        public static string HealthCheckDeviceIdDrive => instance.Value.HEALTHCHECK_DEVICE_ID_DRIVE;
        public static string[] DebugSections => instance.Value.DEBUG_SECTIONS;
    }
}