﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;

namespace JB2.Core
{
    public static class LogExtension
    {
        class CustomProperties : Dictionary<string, object> { }

        private static void Log(ILogger logger, LogLevel logLevel, string message, Exception exception, string caller, int lineNumber)
        {
            var properties = new CustomProperties
            {
                { "CodePos", $"{Path.GetFileName(caller)}-{lineNumber}" }
            };
            Func<CustomProperties, Exception, string> f1 = (CustomProperties, Exception) => message;
            logger.Log(logLevel, 0, properties, exception, f1);
        }

        public static void Debug(this ILogger logger, string message,
            [CallerFilePath] string caller = "", [CallerLineNumber] int lineNumber = 0)
        {
            Log(logger, LogLevel.Debug, message, null, caller, lineNumber);
        }

        public static void Info(this ILogger logger, string message,
            [CallerFilePath] string caller = "", [CallerLineNumber] int lineNumber = 0)
        {
            Log(logger, LogLevel.Information, message, null, caller, lineNumber);
        }

        public static void Warning(this ILogger logger, string message,
            [CallerFilePath] string caller = "", [CallerLineNumber] int lineNumber = 0)
        {
            Log(logger, LogLevel.Warning, message, null, caller, lineNumber);
        }

        public static void Error(this ILogger logger, string message,
            [CallerFilePath] string caller = "", [CallerLineNumber] int lineNumber = 0)
        {
            Log(logger, LogLevel.Error, message, null, caller, lineNumber);
        }

        public static void Exception(this ILogger logger, Exception excep,
            [CallerFilePath] string caller = "", [CallerLineNumber] int lineNumber = 0)
        {
            //Because the lacked information of StorageException, should log ExtendedErrorInformation for better investigation
            var ex = excep as Microsoft.WindowsAzure.Storage.StorageException;
            if (ex?.RequestInformation?.ExtendedErrorInformation != null)
            {
                var details = ex.RequestInformation.ExtendedErrorInformation;
                var message = $"StorageException: ErrorCode={details.ErrorCode}, ErrorMessage={details.ErrorMessage}";
                Log(logger, LogLevel.Error, message, null, caller, lineNumber);
            }
            ResourceManager rm = new ResourceManager("en-US", Assembly.GetExecutingAssembly());
            Log(logger, LogLevel.Error, rm.GetString("", CultureInfo.CurrentUICulture), excep, caller, lineNumber);
        }

        private class DeviceLogger : ILogger
        {
            public ILogger _logger;
            public string _deviceId;

            public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
            {
                var properties = state as CustomProperties;
                if (properties != null)
                {
                    properties.Add("DeviceId", _deviceId);
                }
                _logger.Log(logLevel, eventId, state, exception, formatter);
            }
            public bool IsEnabled(LogLevel logLevel) => _logger.IsEnabled(logLevel);
            public IDisposable BeginScope<TState>(TState state) => _logger.BeginScope(state);
        }

        public static ILogger AddDeviceIdProp(this ILogger logger, string deviceId)
        {
            if (logger is DeviceLogger)
            {
                (logger as DeviceLogger)._deviceId = deviceId;
                return logger;
            }
            return new DeviceLogger
            {
                _logger = logger,
                _deviceId = deviceId
            };
        }
    }
}