﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.ApplicationInsights;
using Microsoft.ApplicationInsights.DataContracts;

namespace JB2.Core
{
    public class MetricAggerator
    {
        private readonly string procedureName;

        private readonly List<double> metricValues = new List<double>();

        private bool Running { get; set; } = true;
        public MetricAggerator(string name, string instrusmentionKey):this(name, instrusmentionKey,30){}
        public MetricAggerator(string name, string instrusmentionKey, int intervalSecond)
        {
            procedureName = name;
            var tc = new TelemetryClient
            {
                InstrumentationKey = instrusmentionKey
            };

            Task.Factory.StartNew(async () =>
            {
                while (Running)
                {
                    await Task.Delay(TimeSpan.FromSeconds(intervalSecond)).ConfigureAwait(false);
                    var metric = Aggregate();
                    if (metric != null)
                    {
                        tc.TrackMetric(metric);
                    }
                }
            }, TaskCreationOptions.LongRunning);
        }
        


        private readonly object lockObj = new object();
        private MetricTelemetry Aggregate()
        {
            lock (lockObj)
            {
                if (!metricValues.Any())
                {
                    return null;
                }

                var metric = new MetricTelemetry
                {
                    Timestamp = DateTime.UtcNow,
                    Name = procedureName,
                    Count = metricValues.Count,
                    Max = metricValues.Max(),
                    Min = metricValues.Min(),
                    Sum = metricValues.Sum(),
                };

                metricValues.Clear();
                return metric;
            }
        }

        public void Add(Stopwatch sw)
        {
            if(sw == null)
            {
                sw = new Stopwatch();
            }
            var value = sw.ElapsedMilliseconds;
            sw.Stop();
            lock (lockObj)
            {
                metricValues.Add(value);
            }
        }

        public void Stop()
        {
            Running = false;
        }
    }

    public class GlobalMetricAggerator
    {
        private readonly Lazy<MetricAggerator> metricLazy;

        public GlobalMetricAggerator(string name)
        {
            metricLazy = new Lazy<MetricAggerator>(() => new MetricAggerator(name, AppEnvironment.AppInsightsKey));
        }

        public void Add(Stopwatch sw)
        {
            metricLazy.Value.Add(sw);
        }
    }
}
