﻿using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace JB2.Core
{
    public static class TelemetryData
    {
        public static readonly string DatabaseName = "jb2-db";
        public static readonly string CollectionName = "TelemetryData";
        public static readonly string DateFormat = "yyyyMMdd_HHmmss";
    }

    public abstract class BaseData
    {
        public abstract string CreateId();
        public abstract string MessageType();
    }

    public class TelemetryData<T> where T : BaseData
    {
        public TelemetryData()
        {
        }

        public TelemetryData(string deviceId, string sdId, long sentTime, long receivedTime, T data, int? timeToLive)
        {
            this.DeviceId = deviceId;
            this.SdId = sdId;
            this.SentTime = sentTime;
            this.ReceivedTime = receivedTime;
            if (data != null)
            {
                this.MessageType = data.MessageType();
                this.Data = data;
                this.Id = data.CreateId();
            }
            this.TimeToLive = timeToLive;
        }

        public string DeviceId { get; set; }
        public string SdId { get; set; }
        public long SentTime { get; set; }
        public long ReceivedTime { get; set; }
        public string MessageType { get; set; }
        public string Id { get; set; }
        public T Data { get; set; }
        [JsonProperty(PropertyName = "ttl", NullValueHandling = NullValueHandling.Ignore)]
        public int? TimeToLive { get; set; }
    }

    public static class TelemetryExtension
    {
        public static IQueryable<TelemetryData<T>> TelemetryQuery<T>(this DocumentClient dbClient, string deviceId) where T : BaseData
        {
            if (dbClient == null) return null;
            var queryOptions = new FeedOptions
            {
                PartitionKey = new PartitionKey(deviceId),
                EnableScanInQuery = false,
                EnableCrossPartitionQuery = false,
                MaxItemCount = -1
            };
            return dbClient.CreateDocumentQuery<TelemetryData<T>>(
                UriFactory.CreateDocumentCollectionUri(TelemetryData.DatabaseName, TelemetryData.CollectionName),
                queryOptions);
        }

        public static async Task UpsertTelemetryAsync<T>(this IDocumentClient dbClient,
            string deviceId, string sdId, long sentTime, long receivedTime, T data) where T : BaseData

        {
            await UpsertTelemetryAsync(dbClient, deviceId, sdId, sentTime, receivedTime, data, null);
        }
        public static async Task UpsertTelemetryAsync<T>(this IDocumentClient dbClient,
            string deviceId, string sdId, long sentTime, long receivedTime, T data, int? timeToLive) where T : BaseData
        {
            if (dbClient == null) return;
            var telemetryData = new TelemetryData<T>(deviceId, sdId, sentTime, receivedTime, data, timeToLive);
            await dbClient.UpsertDocumentAsync(
                UriFactory.CreateDocumentCollectionUri(TelemetryData.DatabaseName, TelemetryData.CollectionName),
                telemetryData, null, true).ConfigureAwait(false);
        }

        public static async Task CreateTelemetryAsync<T>(this IDocumentClient dbClient,
            string deviceId, string sdId, long sentTime, long receivedTime, T data) where T : BaseData
        {
            await CreateTelemetryAsync(dbClient, deviceId, sdId, sentTime, receivedTime, data, null);
        }
        public static async Task CreateTelemetryAsync<T>(this IDocumentClient dbClient,
            string deviceId, string sdId, long sentTime, long receivedTime, T data, int? timeToLive) where T : BaseData
        {
            if (dbClient == null) return;
            var telemetryData = new TelemetryData<T>(deviceId, sdId, sentTime, receivedTime, data, timeToLive);
            await dbClient.CreateDocumentAsync(
                UriFactory.CreateDocumentCollectionUri(TelemetryData.DatabaseName, TelemetryData.CollectionName),
                telemetryData, null, true).ConfigureAwait(false);
        }
    }

}
