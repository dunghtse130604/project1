﻿namespace JB2.Core
{
    public class Setting : BaseData
    {
        public long Time { get; set; }

        public override string CreateId()
        {
            //Return as reset_<time:YYYYMMDD_hhmmss>
            return "setting_" + Time.ToDatetimeString(TelemetryData.DateFormat);
        }
        public override string MessageType()
        {
            return "setting";
        }
    }
}
