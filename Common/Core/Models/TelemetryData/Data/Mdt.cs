﻿namespace JB2.Core
{
    public class Mdt : BaseData
    {
        public long Time { get; set; }
        public Video Video { get; set; }

        public override string CreateId()
        {
            //Return as mdt_<time:YYYYMMDD_hhmmss>
            return "mdt_" + Time.ToDatetimeString(TelemetryData.DateFormat);
        }
        public override string MessageType()
        {
            return "mdt";
        }
    }
}
