﻿namespace JB2.Core
{
    public class Log : BaseData
    {
        public long Time { get; set; }
        public int Index { get; set; }
        public int Type { get; set; }
        public string Message { get; set; }
        public int Error { get; set; }

        public override string CreateId()
        {
            //Return as log_<time:YYYYMMDD_hhmmss>
            return $"log_{Time.ToDatetimeString(TelemetryData.DateFormat)}_{Type}";
        }
        public override string MessageType()
        {
            return "log";
        }
    }
}
