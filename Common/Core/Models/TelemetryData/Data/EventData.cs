﻿namespace JB2.Core
{
    public class MyEventData : Event
    {
        public MyData Data { get; set; }

        public override string CreateId()
        {
            //Return as eventData_<time:YYYYMMDD_hhmmss>
            return "eventData_" + Time.ToDatetimeString(TelemetryData.DateFormat);
        }
        public override string MessageType()
        {
            return "eventData";
        }
    }
}
