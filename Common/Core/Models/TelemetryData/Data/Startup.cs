﻿namespace JB2.Core
{
    public class Startup : BaseData
    {
        public long Time { get; set; }
        public Gps Gps { get; set; }
        public string Fw { get; set; }
        public string Phone { get; set; }
        public Sensor Sensor { get; set; }
        public int VpcReset { get; set; }
        public Fcw Fcw { get; set; }

        public override string CreateId()
        {
            //Return as startup_<time:YYYYMMDD_hhmmss>
            return "startup_" + Time.ToDatetimeString(TelemetryData.DateFormat);
        }
        public override string MessageType()
        {
            return "startup";
        }
    }
}
