﻿namespace JB2.Core
{
    public class Reset : BaseData
    {
        public long Time { get; set; }
        public Gps Gps { get; set; }
        public string Photo { get; set; }

        public override string CreateId()
        {
            //Return as reset_<time:YYYYMMDD_hhmmss>
            return "reset_" + Time.ToDatetimeString(TelemetryData.DateFormat);
        }
        public override string MessageType()
        {
            return "reset";
        }
    }
}
