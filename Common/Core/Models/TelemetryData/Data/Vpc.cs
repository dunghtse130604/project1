﻿namespace JB2.Core
{
    public class Vpc : BaseData
    {
        public long Time { get; set; }
        public Gps Gps { get; set; }
        public Result Result { get; set; }
        public string Photo { get; set; }

        public override string CreateId()
        {
            //Return as vpc_<time:yyyyMMdd_HHmmss>
            return "vpc_" + Time.ToDatetimeString(TelemetryData.DateFormat);
        }
        public override string MessageType()
        {
            return "vpc";
        }
    }
}
