﻿namespace JB2.Core
{
    public class Drive : BaseData
    {
        public long EngineOnTime { get; set; }
        public long Time { get; set; }
        public int Duration { get; set; }
        public LastGps LastGps { get; set; }

        public override string CreateId()
        {
            //Return as drive_<time:YYYYMMDD_hhmmss>
            return "drive_" + Time.ToDatetimeString(TelemetryData.DateFormat);
        }
        public override string MessageType()
        {
            return "drive";
        }
    }
}
