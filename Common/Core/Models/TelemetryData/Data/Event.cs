﻿namespace JB2.Core
{
    public class Event : BaseData
    {
        public long Time { get; set; }
        public Gps Gps { get; set; }
        public Flags Flags { get; set; }
        public int Strength { get; set; }
        public string Photo { get; set; }

        public override string CreateId()
        {
            //Return as event_<time:YYYYMMDD_hhmmss>
            return "event_" + Time.ToDatetimeString(TelemetryData.DateFormat);
        }
        public override string MessageType()
        {
            return "event";
        }
    }
}
