﻿namespace JB2.Core
{
    public class Error : BaseData
    {
        public long Time { get; set; }
        public int Status { get; set; }
        public override string CreateId()
        {
            //Return as error_<time:YYYYMMDD_hhmmss>
            return "error_" + Time.ToDatetimeString(TelemetryData.DateFormat);
        }
        public override string MessageType()
        {
            return "error";
        }
    }
}
