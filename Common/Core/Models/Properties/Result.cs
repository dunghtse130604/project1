﻿namespace JB2.Core
{
    public class Result
    {
        public int Status { get; set; }
        public int vp_x { get; set; }
        public int vp_y { get; set; }
        public int be_p { get; set; }
        public int hd_vp_x { get; set; }
        public int hd_vp_y { get; set; }
        public int hd_be_p { get; set; }
    }
}
