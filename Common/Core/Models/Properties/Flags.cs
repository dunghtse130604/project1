﻿namespace JB2.Core
{
    public class Flags
    {
        public int Button { get; set; }
        public int Shock { get; set; }
        public int Accel { get; set; }
        public int Brake { get; set; }
        public int Turn { get; set; }
        public int Fcw { get; set; }
    }
}
