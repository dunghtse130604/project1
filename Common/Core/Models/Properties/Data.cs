﻿namespace JB2.Core
{
    public class MyData
    {
        public long Time { get; set; }
        public int Duration { get; set; }
        public string Video { get; set; }
        public string Sensor { get; set; }
    }
}
