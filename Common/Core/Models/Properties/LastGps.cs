﻿using Newtonsoft.Json;

namespace JB2.Core
{
    public class LastGps
    {
        public long Time { get; set; }

        [JsonConverter(typeof(DecimalJsonConverter))]
        public double Latitude { get; set; }

        [JsonConverter(typeof(DecimalJsonConverter))]
        public double Longitude { get; set; }
        public int Speed { get; set; }
        public int Heading { get; set; }
    }
}
