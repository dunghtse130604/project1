﻿namespace JB2.Core
{
    public class Video
    {
        public int Width { get; set; }
        public int Height { get; set; }
        public int Fps { get; set; }
        public int Bitrate { get; set; }
    }
}
