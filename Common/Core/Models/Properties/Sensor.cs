﻿namespace JB2.Core
{
    public class Sensor
    {
        public int Calibration { get; set; }
        public int Angle { get; set; }
    }
}
