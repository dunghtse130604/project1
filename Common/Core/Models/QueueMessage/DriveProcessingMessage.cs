﻿namespace JB2.Core
{
    public class DriveProcessingMessage
    {
        public string DeviceId { get; set; }
        public string SdId { get; set; }
        public long Time { get; set; }
        public string Uuid { get; set; }
        public long EngineOnTime { get; set; }

        public override string ToString()
        {
            return $"deviceId: {DeviceId}, sdId: {SdId}, time: {Time}, uuid: {Uuid}, engineOnTime: {EngineOnTime}";
        }
    }
}
