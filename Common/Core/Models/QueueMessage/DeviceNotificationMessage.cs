﻿namespace JB2.Core
{
    public class DeviceNotificationMessage
    {
        public static class ActionType
        {
            public const string OnInitialization = "OnInitialization";
            public const string OnSetting = "OnSetting";
            public const string OnMdtRequest = "OnMdtRequest";
            public const string OnFota = "OnFota";
        }

        public static readonly string MessageType = "messageType";

        public string DeviceId { get; set; }
        public string SdId { get; set; }
        public string actionType { get; set; }
        public string Parameter { get; set; }

        public override string ToString()
        {
            return $"deviceId: {DeviceId}, actionType: {actionType}, parameter: {Parameter}";
        }
    }
}
