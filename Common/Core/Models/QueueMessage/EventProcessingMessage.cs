﻿namespace JB2.Core
{
    public class EventProcessingMessage
    {
        public string DeviceId { get; set; }
        public long FromTime { get; set; }
        public long ToTime { get; set; }
        public string RequestSource { get; set; }
    }
}
