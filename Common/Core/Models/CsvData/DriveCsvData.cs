﻿using System;
using System.Collections.Generic;

namespace JB2.Core
{
    public class DriveCsvData : BaseCsv
    {
        public static class Index
        {
            public static int DeviceId => 0;
            public static int SdId => 1;
            public static int EngineOnTime => 2;
            public static int Time => 3;
            public static int GpsValid => 4;
            public static int GpsLatitude => 5;
            public static int GpsLongitude => 6;
            public static int GpsSpeed => 7;
            public static int GpsHeading => 8;
            public static int GAveX => 9;
            public static int GAveY => 10;
            public static int GAveZ => 11;
            public static int GMaxX => 12;
            public static int GMaxY => 13;
            public static int GMaxZ => 14;
            public static int GMinX => 15;
            public static int GMinY => 16;
            public static int GMinZ => 17;
            public static int GyroAveX => 18;
            public static int GyroAveY => 19;
            public static int GyroAveZ => 20;
        }
        public const int MaxNum = 21;

        public long Time => long.Parse(Cells[Index.Time]);
        public int GpsValid => int.Parse(Cells[Index.GpsValid]);
        public double GpsLatitude => double.Parse(Cells[Index.GpsLatitude]);
        public double GpsLongitude => double.Parse(Cells[Index.GpsLongitude]);
        public int GpsSpeed => int.Parse(Cells[Index.GpsSpeed]);
        public int GpsHeading => int.Parse(Cells[Index.GpsHeading]);
    }

    public class OutputDriveCsvData : BaseCsv
    {
        public override string Header {
            get { return 
                    "deviceId" +
                    "\tsdId\tengineOnTime" +
                    "\ttime\tgps.valid" +
                    "\tgps.latitude\tgps.longitude" +
                    "\tgps.speed\tgps.heading" +
                    "\test.speed\test.fbAccel" +
                    "\test.lrAccel\tg.ave.x" +
                    "\tg.ave.y\tg.ave.z" +
                    "\tg.max.x\tg.max.y" +
                    "\tg.max.z\tg.min.x" +
                    "\tg.min.y\tg.min.z\n"; }
        }

        public static class Index
        {
            public static int DeviceId => 0;
            public static int SdId => 1;
            public static int EngineOnTime => 2;
            public static int Time => 3;
            public static int GpsValid => 4;
            public static int GpsLatitude => 5;
            public static int GpsLongitude => 6;
            public static int GpsSpeed => 7;
            public static int GpsHeading => 8;
            public static int EstSpeed => 9;
            public static int EstFBAccel => 10;
            public static int EstLRAccel => 11;
            public static int GAveX => 12;
            public static int GAveY => 13;
            public static int GAveZ => 14;
            public static int GMinX => 15;
            public static int GMinY => 16;
            public static int GMinZ => 17;
            public static int GMaxX => 18;
            public static int GMaxY => 19;
            public static int GMaxZ => 20;
        }
        public const int MaxNum = 21;

        public OutputDriveCsvData(DriveCsvData srcData, IReadOnlyList<double?> estData)
        {
            ReserveCells(MaxNum);
            if(srcData != null)
            {
                Array.Copy(srcData.Cells, Cells, DriveCsvData.Index.GAveX);
                if(estData != null)
                {
                    Cells[Index.EstSpeed] = FloatString(estData[0]);
                    Cells[Index.EstFBAccel] = FloatString(estData[1]);
                    Cells[Index.EstLRAccel] = FloatString(estData[2]);
                }
                Array.Copy(srcData.Cells, DriveCsvData.Index.GAveX, Cells, Index.GAveX, MaxNum - Index.GAveX);  // TODO: With a bug of wrong order of Min and Max values
            }
        }

        private static string FloatString(double? d)
        {
            return d != null ? d.Value.ToString("0.########") : string.Empty;
        }

    }
}