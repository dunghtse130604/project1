﻿namespace JB2.Core
{
    public class SensorCsvData : BaseCsv
    {
        public override string Header
        {
            get { return "deviceId\tsdId\ttime\tg.x\tg.y\tg.z\tgyro.x\tgyro.y\tgyro.z\n"; }
        }

        public static class Index
        {
            public static int DeviceId => 0;
            public static int SdId => 1;
            public static int Time => 2;
            public static int GX => 3;
            public static int GY => 4;
            public static int GZ => 5;
            public static int GyroX => 6;
            public static int GyroY => 7;
            public static int GyroZ => 8;
        }
        public static int MaxNum => 9;
    }
}
