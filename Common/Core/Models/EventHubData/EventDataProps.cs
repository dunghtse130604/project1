﻿namespace JB2.Core
{
    public static class EventDataProps
    {
        public static string MessageType => "messageType";
        public static string SdId => "sdId";
        public static string SentTime => "sentTime";
        public static string Blob => "blob";
        public static string Content => "content";
        public static string DeviceId => "deviceId";
        public static string DataId => "dataId";
        public static string PoisonCount => "poisonCount";
        public static string ContentSize => "contentSize";
        public static string DataSize => "dataSize";
        public static string MediaSize => "mediaSize";
    }

    public static class MessageType
    {
        public static string Drive => "drive";
        public static string DriveData => "driveData";
        public const string Startup = "startup";
        public const string Event = "event";
        public static string EventData => "eventData";
        public const string Vpc = "vpc";
        public const string Log = "log";
        public const string Log2 = "log2";
        public const string Mdt = "mdt";
        public const string Reset = "reset";
        public const string Error = "error";
        public const string Setting = "setting";    //Abolished type
    }

    public static class DeviceLogType
    {
        public static int Error => 0x1000;
    }
}
