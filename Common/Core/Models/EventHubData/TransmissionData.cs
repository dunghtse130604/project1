﻿using Microsoft.ServiceBus.Messaging;

namespace JB2.Core
{
    public static class TransmissionData
    {
        public static readonly string DateFormat = "yyMMddHHmmss";

        public static EventData Create(byte[] body, string deviceId, string messageType, string sdId, long time)
        {
            return new EventData(body)
            {
                PartitionKey = deviceId,
                Properties =
                {
                    [EventDataProps.MessageType] = messageType,
                    [EventDataProps.DeviceId] = deviceId,
                    [EventDataProps.SdId] = sdId,
                    [EventDataProps.DataId] = $"{deviceId}{time.ToDatetimeString(DateFormat)}"
                }
            };
        }
    }
}
