﻿using Microsoft.WindowsAzure.Storage.Table;

namespace JB2.Core
{
    public class LatestStartupInfo : TableEntity
    {
        public string SdId { get; set; }
        public string Fw { get; set; }
        public string Phone { get; set; }
        public int Calibration { get; set; }
        public int Angle { get; set; }

        public LatestStartupInfo(string deviceId)
        {
            PartitionKey = deviceId;
            RowKey = MessageType.Startup;
        }

        public LatestStartupInfo() { }
    }
}
