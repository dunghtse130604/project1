﻿using Microsoft.WindowsAzure.Storage.Table;

namespace JB2.Core
{
    public static class DeviceInfoSchema
    {
        public static string Valid => "valid";
        public static string NewFw => "newFw";
        public static string LastResetTime => "lastResetTime";
        public static string LastSentDriveTime => "lastSentDriveTime";
        public static string LastReceivedDriveTime => "lastReceivedDriveTime";
        public static string Setting => "setting";
        public static string VideoTime => "videoTime";
        public static string LastSystemLogIndex => "lastSystemLogIndex";
        public static string LastErrorLogIndex => "lastErrorLogIndex";
    }

    public class DeviceInfo<T> : TableEntity
    {
        public DeviceInfo() { }

        public T Value { get; set; }

        public DeviceInfo(string deviceId, string param)
        {
            PartitionKey = deviceId;
            RowKey = param;
        }
        public DeviceInfo(string deviceId, string param, T value)
            : this(deviceId, param)
        {
            this.Value = value;
        }
    }

    public class DeviceVideoTimeProperty : TableEntity
    {
        public DeviceVideoTimeProperty() { }
        public int? Pre { get; set; }
        public int? Post { get; set; }
        public DeviceVideoTimeProperty(string deviceId, string param) : base(deviceId, param) { }
    }
}
