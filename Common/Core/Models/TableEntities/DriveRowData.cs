﻿using Microsoft.WindowsAzure.Storage.Table;

namespace JB2.Core
{
    public class DriveRowData : TableEntity
    {
        public string Content { get; set; }

        public DriveRowData() { }
        public DriveRowData(string deviceId, long time):this(deviceId, time,null) { 
            
        }

        public DriveRowData(string deviceId, long time, string content)
        {
            PartitionKey = deviceId;
            RowKey = time.ToDatetimeString(TelemetryData.DateFormat);
            this.Content = content;
        }
    }
}
