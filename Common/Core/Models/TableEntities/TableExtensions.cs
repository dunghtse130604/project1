﻿using Microsoft.WindowsAzure.Storage.Table;
using Microsoft.WindowsAzure.Storage.Table.Protocol;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JB2.Core
{
    public static class TableExtensions
    {
        public static async Task DeleteEntityAsync<T>(this CloudTable table, T entity) where T : TableEntity
        {
            if(entity != null && table != null)
            {
                entity.ETag = "*";
                await table.ExecuteAsync(TableOperation.Delete(entity)).ConfigureAwait(false);
            }
        }

        public static async Task UpsertEntityAsync<T>(this CloudTable table, T entity) where T : TableEntity
        {
            if(table != null)
            {
                await table.ExecuteAsync(TableOperation.InsertOrReplace(entity)).ConfigureAwait(false);
            }
        }

        public static async Task<T> RetrieveEntityAsync<T>(this CloudTable table, T entity) where T : TableEntity
        {
            if (table == null || entity == null) return default(T);
            var result = (await table.ExecuteAsync(
                TableOperation.Retrieve<T>(entity.PartitionKey, entity.RowKey)).ConfigureAwait(false)).Result;
            return result != null ? (T)result : default(T);
        }

        public static List<T> QueryDeviceEntities<T>(this CloudTable table, string deviceId) where T : ITableEntity, new()
        {
            if (table == null) return new List<T>();
            var query = new TableQuery<T>().Where(TableQuery.GenerateFilterCondition(TableConstants.PartitionKey, QueryComparisons.Equal, deviceId));
            return table.ExecuteQuery(query).ToList();
        }
    }
}
