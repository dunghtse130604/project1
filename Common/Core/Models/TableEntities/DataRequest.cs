﻿using Microsoft.WindowsAzure.Storage.Table;

namespace JB2.Core
{
    public class DataRequest : TableEntity
    {
        public long FromTime { get; set; }
        public long ToTime { get; set; }
        public string RequestSource { get; set; }

        public DataRequest() { }

        public DataRequest(string deviceId, long fromTime, long toTime, string requestSource)
        {
            PartitionKey = deviceId;
            RowKey = fromTime + "_" + toTime;
            this.FromTime = fromTime;
            this.ToTime = toTime;
            this.RequestSource = requestSource;
        }
    }
}
