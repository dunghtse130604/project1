﻿using Microsoft.WindowsAzure.Storage.Table;

namespace JB2.Core
{
    public class SentDataFrame : TableEntity
    {
        public string Uuid { get; set; }

        public SentDataFrame(string deviceId, string time)
        {
            PartitionKey = deviceId;
            RowKey = time;
        }
        public SentDataFrame(string deviceId, string time, string uuid)
            : base(deviceId, time)
        {
            this.Uuid = uuid;
        }

        public SentDataFrame() { }
    }
}
