﻿using Microsoft.WindowsAzure.Storage.Table;

namespace JB2.Core
{
    public static class SettingSchema
    {
        public static readonly string ADAS = "ADAS";
        public static readonly string VP_X = "VP_X";
        public static readonly string VP_Y = "VP_Y";
        public static readonly string BE_P = "BE_P";
        public static readonly string HD_VP_X = "HD_VP_X";
        public static readonly string HD_VP_Y = "HD_VP_Y";
        public static readonly string HD_BE_P = "HD_BE_P";
        public static readonly string CAR_HEIGHT = "CAR_HEIGHT";
        public static readonly string FCW_ENABLE = "FCW_ENABLE";

        private static string[] vpcSettings = { VP_X, VP_Y, BE_P, HD_VP_X, HD_VP_Y, HD_BE_P };
        public static string[] VpcSettings
        {
            get { return vpcSettings; }
            set { vpcSettings = value; }
        }
    }

    public class SettingInfo : TableEntity
    {
        public string Section { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }

        public SettingInfo() { }

        public SettingInfo(string deviceId, string section, string key)
        {
            PartitionKey = deviceId;
            RowKey = section + "_" + key;
            this.Section = section;
            this.Key = key;
        }
        public SettingInfo(string deviceId, string section, string key, string value)
            : this(deviceId, section, key)
        {
            this.Value = value;
        }
    }
}
