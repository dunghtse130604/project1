using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;
using Microsoft.ServiceBus.Messaging;
using System;
using System.Threading.Tasks;

namespace JB2.Core
{
    public abstract class EventHubTriggerHandler
    {
        protected JB2Message Message;
        protected ILogger Logger;

        protected abstract Task Run();

        public async Task ExecuteAsync(JB2Message msg, ILogger logger, IAsyncCollector<EventData> poison)
        {
            Message = msg;
            Logger = logger;
            var retries = new RetryPolicy<RetryAllExceptionStrategy>(3, TimeSpan.FromSeconds(5));
            try
            {
                await retries.ExecuteAsync(Run).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
                if (ex is ArgumentException)
                {
                    Logger.Error("Non transient exception.");
                    return;
                }

                var poisonCount = msg.PoisonCount;
                if (poisonCount >= 2)
                {
                    Logger.Error("Not throw to poison EventHub due to 3rd failure");
                    return;
                }
                msg.PoisonCount = poisonCount + 1;
                
                // System property will be removed when added into poison EventHub, so set deviceId into App property.
                msg.Properties[EventDataProps.DeviceId] = msg.DeviceId;

                try
                {
                    await poison.PushEventDataAsync(msg.Data).ConfigureAwait(false);
                    Logger.Error("Throw message to poison EventHub");
                }
                catch (Exception ex2)
                {
                    Logger.Exception(ex2);
                    Logger.Error("Failed to throw to poison EventHub");
                }
            }
        }

        private class RetryAllExceptionStrategy : ITransientErrorDetectionStrategy
        {
            public bool IsTransient(Exception ex)
            {
                return !(ex is ArgumentException);
            }
        }
    }
}