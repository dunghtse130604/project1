﻿using Microsoft.Extensions.Logging;
using System;
using System.Data.Common;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace JB2.Core
{
    public static class SecurityUtils
    {
        private const string SignatureHeader = "X-Signature";
        private const int TimeSecMargin = 600;

        public static async Task AddAuthenticationAsync(HttpRequestMessage req, string secretKey)
        {
            // Set Date with current time
            req.Headers.Date = DateTimeOffset.UtcNow;

            // Set Content-MD5 from data
            if (req.Method != HttpMethod.Get)
            {
                req.Content.Headers.ContentMD5 = HashByMd5(await req.Content.ReadAsByteArrayAsync().ConfigureAwait(false));
            }

            // Set signature
            var sigText = ConvertSignatureText(req);
            req.Headers.Add(SignatureHeader, HashByHmacSha256(secretKey, sigText));
        }

        public static async Task<bool> IsAuthenticatedAsync(HttpRequestMessage req, ILogger logger)
        {
            // Check must headers existing
            if (!req.Headers.Contains(SignatureHeader) || req.Headers.Date == null ||
                (req.Method != HttpMethod.Get && req.Content.Headers.ContentMD5 == null))
            {
                logger.Warning("Some required headers not included");
                return false;
            }

            // Check difference between date header and system time is less than TimeSecMargin
            TimeSpan diff = DateTimeOffset.Now.Subtract(req.Headers.Date.Value);
            if (Math.Abs(diff.TotalSeconds) > TimeSecMargin)
            {
                logger.Warning("Date is invalid: " + req.Headers.Date.Value);
                return false;
            }

            // Check Content-MD5
            if (req.Content.Headers.ContentMD5 != null)
            {
                byte[] md5Hash = HashByMd5(await req.Content.ReadAsByteArrayAsync().ConfigureAwait(false));
                if (!req.Content.Headers.ContentMD5.SequenceEqual(md5Hash))
                {
                    logger.Warning("ContentMD5 is invalid");
                    return false;
                }
            }

            // Check Signature by primary and secondary keys in each
            string signature = req.Headers.GetValues(SignatureHeader).SingleOrDefault();
            string sigText = ConvertSignatureText(req);
            logger.Debug("Signature text: " + sigText);
            string[] accessKeys =
            {
                AppEnvironment.RestApiAccessKey1,
                AppEnvironment.RestApiAccessKey2
            };
            foreach (var key in accessKeys)
            {
                if (!string.IsNullOrEmpty(key) && HashByHmacSha256(key, sigText) == signature)
                {
                    return true;
                }
            }
            logger.Warning("Key validation is invalid");
            return false;
        }

        public static string GenerateAuthenKey(string ioTConnectionString, string deviceId)
        {
            var hostName = new DbConnectionStringBuilder { ConnectionString = ioTConnectionString }["HostName"].ToString();
            return HashByHmacSha256(Convert.ToBase64String(Encoding.UTF8.GetBytes(hostName)), deviceId);
        }

        private static string ConvertSignatureText(HttpRequestMessage req)
        {
            string date = req.Headers.GetValues("Date").SingleOrDefault();
            string contentMd5 = string.Empty;
            if (req.Content?.Headers.ContentMD5 != null)
            {
                contentMd5 = req.Content.Headers.GetValues("Content-MD5").SingleOrDefault();
            }
            return $"{req.Method}\n{req.RequestUri.PathAndQuery}\n{date}\n{contentMd5}";
        }

        private static byte[] HashByMd5(byte[] raw)
        {
            using (MD5 md5 = MD5.Create())
            {
                return md5.ComputeHash(raw);
            }
        }

        private static string HashByHmacSha256(string keyAsBase64, string text)
        {
            using (var sha256 = new HMACSHA256(Convert.FromBase64String(keyAsBase64)))
            {
                var macBytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(text));
                return Convert.ToBase64String(macBytes);
            }
        }
    }
}