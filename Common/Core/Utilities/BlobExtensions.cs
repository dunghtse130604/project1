﻿using Microsoft.WindowsAzure.Storage.Blob;
using System.IO;
using System.Threading.Tasks;

namespace JB2.Core
{
    public static class BlobExtensions
    {
        public static byte[] DownloadByteArray(this CloudBlockBlob blob)
        {
            using (var stream = new MemoryStream())
            {
                blob.DownloadToStream(stream);
                return stream.ToArray();
            }
        }

        public static async Task<byte[]> DownloadByteArrayAsync(this CloudBlockBlob blob)
        {
            using (var stream = new MemoryStream())
            {
                await blob.DownloadToStreamAsync(stream).ConfigureAwait(false);
                return stream.ToArray();
            }
        }

        public static async Task UploadByteArray(this CloudBlockBlob blob, byte[] buffer)
        {
            await blob.UploadFromByteArrayAsync(buffer, 0, buffer.Length).ConfigureAwait(false);
        }

        public static string GetBlobPath(this CloudBlockBlob blob)
        {
            return blob.Uri.AbsolutePath.Remove(0, 1);
        }
    }
}