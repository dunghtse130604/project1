﻿using System;

namespace JB2.Core
{
    public static class UnixTime
    {
        public static long Now => DateTimeOffset.UtcNow.ToUnixTimeSeconds();

        public static long From(DateTimeOffset date)
        {
            return date.ToUnixTimeSeconds();
        }

        public static string ToDatetimeString(this long unixTime, string format)
        {
            return DateTimeOffset.FromUnixTimeSeconds(unixTime).ToString(format);
        }

        public static string ToJstDateTimeString(this long unixTime, string format)
        {
            return DateTimeOffset.FromUnixTimeSeconds(unixTime).ToOffset(TimeSpan.FromHours(9)).ToString(format);
        }
    }
}