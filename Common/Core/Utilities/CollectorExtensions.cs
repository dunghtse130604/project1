﻿using Microsoft.Azure.WebJobs;
using System.Threading.Tasks;

namespace JB2.Core
{
    public static class CollectorExtensions
    {
        public static async Task PushEventDataAsync<T>(this IAsyncCollector<T> collector, T data)
        {
            await collector.AddAsync(data).ConfigureAwait(false);
            await collector.FlushAsync().ConfigureAwait(false);
        }

        public static async Task PushQueueMessageAsync<T>(this IAsyncCollector<T> collector, T data)
        {
            await collector.AddAsync(data).ConfigureAwait(false);
            await collector.FlushAsync().ConfigureAwait(false);
        }
    }
}
