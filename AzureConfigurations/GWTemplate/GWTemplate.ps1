Param(
    [string] $TemplateFile = 'GWTemplate.json',
    [string] $TemplateParametersFile = 'GWTemplate.parameters.json',
    [string] [parameter(Mandatory = $true)] $SubscriptionName,
    [string] [Parameter(Mandatory = $true)] $ResourceGroupName,
    [string] [Parameter(Mandatory = $false)] $ResourceGroupLocation = "Japan East",
    [string] [ValidateLength(3,8)] [parameter(Mandatory = $true)] $Prefix,
    [string] [parameter(Mandatory = $true)] $UserAzure,
    [securestring] [parameter(Mandatory = $true)] $PassAzure
)

$ErrorActionPreference = 'Stop'

# Check if module AzureRM installed
$AzureRMVersion = (Get-Module -ListAvailable -Name AzureRM -Refresh).Version.Major
if ($AzureRMVersion) {
    Write-Host "[v] AzureRM installed."
}
else {
    Write-Host "[x]Please install using [Install-Module AzureRM] and try again."
    exit
}

# - MsBuild component.
$msBuildPath = "C:\Program Files (x86)\MSBuild\14.0\Bin\MSBuild.exe"
if (Test-Path $msBuildPath) {
    Write-Output "[v] MsBuild Path : $msBuildPath"
}
else {
    Write-Warning "[x] Please install MsBuild and try again."
    exit
}

# Login to Azure
$Login = Login-AzureRmAccount -Credential (New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $UserAzure, $PassAzure)
if ($Login) {
    Write-Host "[v] Logged in."
}
else {
    Write-Host "[x] Login Fail."
    exit
}

# Change the subscription
$Subscription = Select-AzureRmSubscription -SubscriptionName "$SubscriptionName"
if ($Subscription) {
    Write-Host "[v] Change the subscription."
}
else {
    Write-Host "[x] Change the subscription Fail."
    exit
}

# Create or update the resource group using the specified template file and template parameters file
$ResourceGroup = Get-AzureRmResourceGroup -Name $ResourceGroupName -ErrorAction SilentlyContinue
if ($ResourceGroup) {
    Write-Output "Using existing resource group '$ResourceGroupName'"
}
else {
    Write-Output "Could not find resource group '$ResourceGroupName' - will create it"
    Write-Output "Creating resource group '$ResourceGroupName' in location '$ResourceGroupLocation'"
    New-AzureRmResourceGroup -Name $ResourceGroupName -Location $ResourceGroupLocation -Verbose -Force
}

# Get Path of TemplateFile and TemplateParametersFile
$TemplateFile = [System.IO.Path]::GetFullPath([System.IO.Path]::Combine($PSScriptRoot, $TemplateFile))
$TemplateParametersFile = [System.IO.Path]::GetFullPath([System.IO.Path]::Combine($PSScriptRoot, $TemplateParametersFile))

# Start the template deployment
Write-Output "Starting GW template deployment"
if ( (Test-Path $TemplateFile) -and (Test-Path $TemplateParametersFile) ) {
    $ParamContent = Get-Content $TemplateParametersFile | ConvertFrom-Json
    $ParamContent.parameters.prefix.value = $Prefix
    $ParamContent | ConvertTo-Json  | set-content $TemplateParametersFile

    New-AzureRmResourceGroupDeployment -Name ((Get-ChildItem $TemplateFile).BaseName + '-' + ((Get-Date).ToUniversalTime()).ToString('MMdd-HHmm')) `
        -ResourceGroupName $ResourceGroupName `
        -TemplateFile $TemplateFile `
        -TemplateParameterFile $TemplateParametersFile `
        -Force -Verbose `
        -ErrorVariable ErrorMessages
    if ($ErrorMessages) {
        Write-Output '', 'Template deployment returned the following errors:', @(@($ErrorMessages) | ForEach-Object { $_.Exception.Message.TrimEnd("`r`n") })
    }
    else {
        # Create prefix to resource group
        Set-AzureRmResourceGroup -Name $ResourceGroupName -Tag @{ prefix=$Prefix }
        
        #Table Storage:
        $StorageAccountName = $Prefix.replace("-","") + 'app'
        # Create 8 blob container(storage-app)
        $StorageAccount = Get-AzureRmStorageAccount -ResourceGroupName $ResourceGroupName -Name $StorageAccountName
        # Create "iothub" container form TemplateGW.json template
        #New-AzureStorageContainer -Name "iothub" -Context $StorageAccount.Context
        New-AzureStorageContainer -Name "drivedata" -Context $StorageAccount.Context
        New-AzureStorageContainer -Name "firmware" -Context $StorageAccount.Context -Permission blob
        New-AzureStorageContainer -Name "healthcheck" -Context $StorageAccount.Context
        New-AzureStorageContainer -Name "photo" -Context $StorageAccount.Context
        New-AzureStorageContainer -Name "sensor" -Context $StorageAccount.Context
        New-AzureStorageContainer -Name "sensordata" -Context $StorageAccount.Context
        New-AzureStorageContainer -Name "video" -Context $StorageAccount.Context
        New-AzureStorageContainer -Name "kittinglog" -Context $StorageAccount.Context
        # Create 4 table storage(storage-app)
        "DataRequest DeviceInfo DriveRowData LatestInfo SentDataFrame SettingInfo".split() | New-AzureStorageTable -Context $StorageAccount.Context

        # DocumentDB
        $DocDBAccountName = $Prefix + '-db'
        $DatabaseName = "jb2-db"
        # Create index, 1 document(documentDB)
        $DocumentEndpoint = (Get-AzureRmResource -ResourceType "Microsoft.DocumentDb/databaseAccounts" -ResourceName $DocDBAccountName -ResourceGroupName $ResourceGroupName).Properties.documentEndpoint
        $Keys = Invoke-AzureRmResourceAction -Action listKeys -ResourceType "Microsoft.DocumentDb/databaseAccounts" -ApiVersion "2015-04-08" -ResourceName $DocDBAccountName -ResourceGroupName $ResourceGroupName -Force
        $PrimaryMasterKey = $Keys.primaryMasterKey

        $currentPath = (Split-Path -Parent $MyInvocation.MyCommand.Path)
        $nugetPath = $currentPath.Replace("AzureConfigurations\GWTemplate", "Build\BuildTools\nuget.exe")
        $solutionPath = $currentPath.Replace("AzureConfigurations\GWTemplate", "AzureConfigurations\CosmosDB\CosmosDB-Tool")
        $slnPath = $currentPath.Replace("AzureConfigurations\GWTemplate", "AzureConfigurations\CosmosDB\CosmosDB.sln")
        $documentDBFile = Join-Path $solutionPath "bin\Release\CreateCosmosDB.exe"

        & $nugetPath restore $slnPath
        $msBuild = & $msBuildPath "$slnPath" /t:Clean /t:Rebuild /property:Configuration=Release /property:Platform="Any CPU" /p:TargetFramework=v4.6.2
        $msBuildResult = $msBuild | Select-String -Pattern "Build succeeded"

        if ($msBuildResult) {
            Set-Location $solutionPath
            & $documentDBFile -e $DocumentEndpoint -p $PrimaryMasterKey -d $DatabaseName
        }
        else {
            Write-Warning ("[x] Build cosmosDB failed ")
        }
    }
}