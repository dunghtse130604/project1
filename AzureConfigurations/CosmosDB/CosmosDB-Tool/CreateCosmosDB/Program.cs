﻿using System;
using System.Collections.Generic;
using System.IO;
using CommandLine;
using Microsoft.Azure.Documents.Client;
using Newtonsoft.Json;

namespace CreateDocumentDB
{
    class Program
    {
        static bool InitOption(Options options, string[] args)
        {
            // Parser option value
            var parser = new Parser(s =>
            {
                s.MutuallyExclusive = true;
                s.CaseSensitive = true;
                s.HelpWriter = Console.Error;
            });
            return parser.ParseArguments(args, options);

        }

        public static void Main(string[] args)
        {
            var options = new Options();
            if (!InitOption(options, args)) return;
            string dbName = options.DbName;
            DocumentClient client = new DocumentClient(new Uri(options.EndpointUrl), options.PrimaryKey);

            Console.WriteLine("EndpointUrl: " + options.EndpointUrl);
            Console.WriteLine("PrimaryKey: " + options.PrimaryKey);
            Console.WriteLine("dbName: " + options.DbName);

            try
            {
                DocumentDbUtils utils = new DocumentDbUtils();
                utils.CreateDatabase(client, dbName).Wait();
                Console.WriteLine("Create database success: " + dbName);

                using (StreamReader r = new StreamReader("CreateCosmosDB\\configuration.json"))
                {
                    string json = r.ReadToEnd();
                    List<DocumentObject> items = JsonConvert.DeserializeObject<List<DocumentObject>>(json);
                    foreach (var item in items)
                    {
                        utils.CreateIndexParCollection(client, dbName, item.Name, item.PartitionKey, item.Index, item.Throughput).Wait();
                        Console.WriteLine("Create " + item.Name);
                    }
                }
            }
            catch (Exception e)
            {
                Exception baseException = e.GetBaseException();
                Console.WriteLine("Error: {0}, Message: {1}", e.Message, baseException.Message);
            }
            finally
            {
                Console.WriteLine("End of demo, press any key to exit.");
            }
        }

    }
}
