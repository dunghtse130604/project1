﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace CreateDocumentDB
{
    public class DocumentObject
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("partitionKey")]
        public string PartitionKey { get; set; }

        [JsonProperty("throughput")]
        public int Throughput { get; set; }

        [JsonProperty("index")]
        public List<string> Index { get; set; }

    }
}
