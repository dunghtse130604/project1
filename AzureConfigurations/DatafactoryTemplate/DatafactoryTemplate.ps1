﻿Param(
    [string] $TemplateFile = 'DatafactoryTemplate.json',
    [string] $TemplateParametersFile = 'DatafactoryTemplate.parameters.json',
    [string] [parameter(Mandatory = $true)] $SubscriptionName,
    [string] [Parameter(Mandatory = $true)] $ResourceGroupName,
    [string] [parameter(Mandatory = $true)] $SourceStorageAccount,
    [string] [parameter(Mandatory = $true)] $SinkStorageAccount,
    [string] [parameter(Mandatory = $true)] $UserAzure,
    [securestring] [parameter(Mandatory = $true)] $PassAzure,
    [string] [parameter(Mandatory = $true)] $DatafactoryName,
    [string] [Parameter(Mandatory = $false)] $DatafactoryLocation = "Southeast Asia"
)

$ErrorActionPreference = 'Stop'

# Check if module AzureRM installed
$AzureRMVersion = (Get-Module -ListAvailable -Name AzureRM -Refresh).Version.Major
if ($AzureRMVersion) {
    Write-Host "[v] AzureRM installed."
}
else {
    Write-Host "[x]Please install using [Install-Module AzureRM] and try again."
    exit
}

# Login to Azure
$Login = Login-AzureRmAccount -Credential (New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $UserAzure, $PassAzure)
if ($Login) {
    Write-Host "[v] Logged in."
}
else {
    Write-Host "[x] Login Fail."
    exit
}

# Change the subscription
$Subscription = Select-AzureRmSubscription -SubscriptionName "$SubscriptionName"
if ($Subscription) {
    Write-Host "[v] Change the subscription."
}
else {
    Write-Host "[x] Change the subscription Fail."
    exit
}

# Create or update the resource group using the specified template file and template parameters file
$ResourceGroup = Get-AzureRmResourceGroup -Name $ResourceGroupName -ErrorAction SilentlyContinue
if ($ResourceGroup) {
    Write-Output "Using existing resource group '$ResourceGroupName'"
}
else {
    Write-Output "Resource group not found, please check your parameter"
    exit
}

# Get Path of TemplateFile and TemplateParametersFile
$TemplateFile = [System.IO.Path]::GetFullPath([System.IO.Path]::Combine($PSScriptRoot, $TemplateFile))
$TemplateParametersFile = [System.IO.Path]::GetFullPath([System.IO.Path]::Combine($PSScriptRoot, $TemplateParametersFile))


# Get connection string from source storage account
$SourceStorage = Get-AzureRMStorageAccount | Where-Object { $_.StorageAccountName -eq $SourceStorageAccount }
if ($SourceStorage){
    $SourceStorageKey = (Get-AzureRmStorageAccountKey -ResourceGroupName $ResourceGroupName -Name $SourceStorageAccount).Value[0]
    $SourceConnectionStr = 'DefaultEndpointsProtocol=https;AccountName={0};AccountKey={1};EndpointSuffix=core.windows.net' -f $SourceStorage.StorageAccountName, $SourceStorageKey
}

$SinkStorage = Get-AzureRMStorageAccount | Where-Object { $_.StorageAccountName -eq $SinkStorageAccount }
if ($SinkStorage){
    $SinkStorageKey = (Get-AzureRmStorageAccountKey -ResourceGroupName $ResourceGroupName -Name $SinkStorageAccount).Value[0]
    $SinkConnectionStr = 'DefaultEndpointsProtocol=https;AccountName={0};AccountKey={1};EndpointSuffix=core.windows.net' -f $SinkStorage.StorageAccountName, $SinkStorageKey
}

# Set connection string to file template parameter
if ( (Test-Path $TemplateFile) -and (Test-Path $TemplateParametersFile) ) {
    $ParamContent = Get-Content $TemplateParametersFile | ConvertFrom-Json
    foreach ($property in $ParamContent.parameters.PSObject.Properties) { 
        
        if($property.Name.Contains("jb2app")){
            if($property.Name.Contains("_Connection_connectionString")){
                $property.value.value = $SourceConnectionStr
            }
        }

        if($property.Name.Contains("jb2bigdata")){
            if($property.Name.Contains("_Connection_connectionString")){
                $property.value.value = $SinkConnectionStr
            }
        }
     }
}

# Set name for data factory
$ParamContent.parameters.factoryName.value = $DatafactoryName

# Set subscription for data facatory
$ParamContent.parameters.factorySubscription.value = $SubscriptionName

# Set resource group for data factory
$ParamContent.parameters.factoryResourceGroup.value = $ResourceGroupName

# Set location for data factory
$ParamContent.parameters.factoryLocation.value = $DatafactoryLocation

$ParamContent | ConvertTo-Json  | set-content $TemplateParametersFile

# Create data factory
$DataFactory = Set-AzureRmDataFactoryV2 -ResourceGroupName $ResourceGroupName -Location $DatafactoryLocation -Name $DatafactoryName

# Deploy template to create dataset, pipeline, trigger ... from template
New-AzureRmResourceGroupDeployment -Name MyARMDeployment -ResourceGroupName $ResourceGroupName -TemplateFile $TemplateFile -TemplateParameterFile $TemplateParametersFile