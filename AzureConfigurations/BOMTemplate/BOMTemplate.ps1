#Requires -Version 3.0

Param(
    [string] $TemplateFile = 'BOMTemplate.json',
    [string] $TemplateParametersFile = 'BOMTemplate.parameters.json',
    [string] [parameter(Mandatory = $true)] $SubscriptionName,
    [string] [Parameter(Mandatory = $true)] $ResourceGroupName,
    [string] [Parameter(Mandatory = $false)] $ResourceGroupLocation = "Japan East",
    [string] [parameter(Mandatory = $true)] $Prefix,
    [string] [parameter(Mandatory = $true)] $DBServerAdminLogin,
    [string] [parameter(Mandatory = $true)] $DBServerAdminLoginPassword,
    [string] [parameter(Mandatory = $true)] $UserAzure,
    [securestring] [parameter(Mandatory = $true)] $PassAzure
)

try {
    [Microsoft.Azure.Common.Authentication.AzureSession]::ClientFactory.AddUserAgent("VSAzureTools-$UI$($host.name)".replace(' ', '_'), '3.0.0')
}
catch { }
$ErrorActionPreference = 'Stop'
Set-StrictMode -Version 3

# Check if module AzureRM installed
$AzureRMVersion = (Get-Module -ListAvailable -Name AzureRM -Refresh).Version.Major
if ($AzureRMVersion) {
    Write-Host "[v] AzureRM installed."
}
else {
    Write-Host "[x]Please install using [Install-Module AzureRM] and try again."
    exit
}

# Login to Azure
$Login = Login-AzureRmAccount -Credential (New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $UserAzure, $PassAzure)
if ($Login) {
    Write-Host "[v] Logged in."
}
else {
    Write-Host "[x] Login Fail."
    exit
}

# Change the subscription
$Subscription = Select-AzureRmSubscription -SubscriptionName "$SubscriptionName"
if ($Subscription) {
    Write-Host "[v] Change the subscription."
}
else {
    Write-Host "[x] Change the subscription Fail."
    exit
}

# Create or update the resource group using the specified template file and template parameters file
$ResourceGroup = Get-AzureRmResourceGroup -Name $ResourceGroupName -ErrorAction SilentlyContinue
if ($ResourceGroup) {
    Write-Output "Using existing resource group '$ResourceGroupName'"
}
else {
    Write-Output "Could not find resource group '$ResourceGroupName' - will create it"
    Write-Output "Creating resource group '$ResourceGroupName' in location '$ResourceGroupLocation'"
    New-AzureRmResourceGroup -Name $ResourceGroupName -Location $ResourceGroupLocation -Verbose -Force
}

# Get Path of TemplateFile and TemplateParametersFile
$TemplateFile = [System.IO.Path]::GetFullPath([System.IO.Path]::Combine($PSScriptRoot, $TemplateFile))
$TemplateParametersFile = [System.IO.Path]::GetFullPath([System.IO.Path]::Combine($PSScriptRoot, $TemplateParametersFile))

# Start the template deployment
Write-Output "Starting BOM template deployment"
if ( (Test-Path $TemplateFile) -and (Test-Path $TemplateParametersFile) ) {
    $ParamContent = Get-Content $TemplateParametersFile | ConvertFrom-Json
    $ParamContent.parameters.prefix.value = $Prefix
    $ParamContent.parameters.DBServerAdminLogin.value = $DBServerAdminLogin
    $ParamContent.parameters.DBServerAdminLoginPassword.value = $DBServerAdminLoginPassword
    $ParamContent | ConvertTo-Json  | set-content $TemplateParametersFile

    New-AzureRmResourceGroupDeployment -Name ((Get-ChildItem $TemplateFile).BaseName + '-' + ((Get-Date).ToUniversalTime()).ToString('MMdd-HHmm')) `
        -ResourceGroupName $ResourceGroupName `
        -TemplateFile $TemplateFile `
        -TemplateParameterFile $TemplateParametersFile `
        -Force -Verbose `
        -ErrorVariable ErrorMessages
    if ($ErrorMessages) {
        Write-Output '', 'Template deployment returned the following errors:', @(@($ErrorMessages) | ForEach-Object { $_.Exception.Message.TrimEnd("`r`n") })
    }
}



