**Install custom jar:**
mvn install:install-file -Dfile=.\src\main\resources\calcFeatures.jar -DgroupId=main.naviappli.calcfeatures -DartifactId=calcFeatures -Dpackaging=jar -Dversion=0.0.1

**If you cannot install with command above**
Unzip file main.zip and put it into /.m2/repository/