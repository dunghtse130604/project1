package jb2.drive;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import main.naviappli.calcfeatures.CalcFeatures;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class DriveCalculatorServlet extends HttpServlet {
    private static final String CONTENTTYPE_TEXT = "text/plain;charset=UTF-8";
    private static final String CONTENTTYPE_JSON = "application/json;charset=UTF-8";
	private static final String DRIVE_CALCULATOR_VERSION = "";

    private static final ObjectMapper mapper = new ObjectMapper();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        setResponse(response, HttpServletResponse.SC_OK, "DriveCalculator health check.\r\nVersion: " + DRIVE_CALCULATOR_VERSION, CONTENTTYPE_TEXT);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try (InputStream inputStream = request.getInputStream()) {
            Double[][] coordinates = mapper.readValue(inputStream, Double[][].class);
            List<Double[]> estimatedValues = new ArrayList<>();
            CalcFeatures features = new CalcFeatures();
            Double[] calculatedResult;
            for (Double[] coordinate : coordinates) {
                calculatedResult = features.latestFeatures(coordinate);
                estimatedValues.add(new Double[]{calculatedResult[3], calculatedResult[4], calculatedResult[5]});
            }
            setResponse(response, HttpServletResponse.SC_OK, mapper.writeValueAsString(estimatedValues), CONTENTTYPE_JSON);
        } catch (JsonProcessingException e) {
            setResponse(response, HttpServletResponse.SC_BAD_REQUEST, e.toString(), CONTENTTYPE_TEXT);
        } catch (Exception e) {
            setResponse(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.toString(), CONTENTTYPE_TEXT);
        }
    }

    private void setResponse(HttpServletResponse response, int status, String message, String contentType) throws IOException {
        response.setContentType(contentType);
        response.setStatus(status);
        response.getOutputStream().print(message);
    }
}
