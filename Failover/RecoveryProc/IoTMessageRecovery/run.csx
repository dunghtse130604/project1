﻿#r "Microsoft.ServiceBus"
#r "Newtonsoft.Json"
#r "Microsoft.Azure.WebJobs"
using System;
using System.Text;
using Microsoft.Azure.WebJobs;
using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;

public static async Task Run(EventData iotMessageMessagePoison, IAsyncCollector<EventData> outputRetryMessage, TraceWriter log)
{
    var content = $"Properties={JsonConvert.SerializeObject(iotMessageMessagePoison.Properties)}, EnqueuedTimeUtc={iotMessageMessagePoison.EnqueuedTimeUtc}";
    log.Info($"[MessageRecovery] {content}");

    await outputRetryMessage.AddAsync(iotMessageMessagePoison);
    await outputRetryMessage.FlushAsync();
}