﻿using JB2.Core;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.WebJobs;
using Microsoft.ServiceBus.Messaging;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Table;

namespace JB2.MainProc
{
    public abstract class BaseMessageProcessor : EventHubTriggerHandler
    {
        // Input data
        public string DeviceId => Message.DeviceId;
        public string SdId => Message.SdId;
        public long SentTime => Message.SentTime;
        public long ReceivedTime => Message.ReceivedTime;
        public string JsonContent => Message.JsonContent;
        public byte[] DataContent => Message.ExtraData;
        public byte[] MediaContent => Message.MediaData;

        // Bindings
        public DocumentClient DbClient { get; set; }
        public CloudTable DeviceInfoTable { get; set; }
        public CloudTable SettingInfoTable { get; set; }
        public CloudTable DataRequestTable { get; set; }
        public CloudTable LatestInfoTable { get; set; }
        public CloudBlobContainer SensorContainer { get; set; }
        public CloudBlobContainer PhotoBlobContainer { get; set; }
        public CloudBlobContainer IoTHubBlobContainer { get; set; }
        public IAsyncCollector<DeviceNotificationMessage> DeviceNotificationQueue { get; set; }
        public IAsyncCollector<EventProcessingMessage> EventDataQueueItems { get; set; }
        public IAsyncCollector<EventData> TransmissionEventHub { get; set; }

        public string FunctionDirectory { get; set; }
    }
}