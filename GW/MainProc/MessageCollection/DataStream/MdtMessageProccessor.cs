﻿using JB2.Core;
using Microsoft.WindowsAzure.Storage;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JB2.MainProc
{
    public class MdtMessageProccessor : BaseMessageProcessor
    {
        protected override async Task Run()
        {
            Logger.Info($"Collect mdt message: {JsonContent}");

            var mdtMessage = JsonConvert.DeserializeObject<Mdt>(JsonContent);
            // [Compatible] Old FW send extra data with one space if no sensor data, so check the data length.
            if (DataContent != null && DataContent.Length > 1)
            {
                var sensorTsvStr = Encoding.UTF8.GetString(GzipUtils.Decompress(DataContent));
                // Upload sensor data to blob
                var csvBlob = SensorContainer.GetBlockBlobReference(
                    $"{DeviceId}/{mdtMessage.Time.ToDatetimeString(TelemetryData.DateFormat)}.csv");
                await csvBlob.UploadTextAsync(sensorTsvStr);
            }
            else
            {
                Logger.Info("No sensor data");
            }

            // Only upload media content if TCU send media in body
            if (MediaContent != null && !Message.HasBlob())
            {
                // Upload h264 media content
                var mediaBlob = IoTHubBlobContainer.GetBlockBlobReference(
                    $"{DeviceId}/{mdtMessage.Time.ToDatetimeString(TelemetryData.DateFormat)}_h264");
                await mediaBlob.UploadByteArray(MediaContent);
            }

            // Update mdt record
            await DbClient.UpsertTelemetryAsync(DeviceId, SdId, SentTime, ReceivedTime, mdtMessage);

            // Get all entry for this deviceId from DataRequest table storage (filter data purpose)
            var dataRequestItems = DataRequestTable.QueryDeviceEntities<DataRequest>(DeviceId);

            var mdtTelemetryList = DbClient.TelemetryQuery<Mdt>(DeviceId)
                .Where(mdt => mdt.ReceivedTime == 0 && mdt.MessageType == MessageType.Mdt)
                .ToList();

            foreach (var requestItem in dataRequestItems)
            {
                try
                {
                    var mdtCnt = mdtTelemetryList.Count(x => x.Data.Time >= requestItem.FromTime &&
                                                             x.Data.Time <= requestItem.ToTime);
                    if (mdtCnt != 0)
                    {
                        Logger.Debug($"[Continue] Request data fromTime toTime is not 0 record: {mdtCnt} records");
                        continue;
                    }


                    // Execute delete
                    await DataRequestTable.DeleteEntityAsync(requestItem);

                    // Add message to EventProcessing queue
                    var eventProcessingMessage = new EventProcessingMessage
                    {
                        DeviceId = DeviceId,
                        FromTime = requestItem.FromTime,
                        ToTime = requestItem.ToTime,
                        RequestSource = requestItem.RequestSource
                    };

                    await EventDataQueueItems.PushQueueMessageAsync(eventProcessingMessage);
                    Logger.Debug($"[{DeviceId}] Processed item: {requestItem.FromTime} - {requestItem.ToTime}");
                }
                catch (StorageException e)
                {
                    // Only ignore if delete non-found record
                    if (e.RequestInformation.HttpStatusCode != 404)
                    {
                        throw;
                    }
                }
            }
        }
    }
}