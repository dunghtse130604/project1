﻿using JB2.Core;
using Microsoft.Azure.Documents;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace JB2.MainProc
{
    public class EventMessageProcessor : BaseMessageProcessor
    {
        protected override async Task Run()
        {
            Logger.Info($"Collect event message: {JsonContent}");

            // Environment value
            var defaultPreTime = ExternalConfig.PreTime;
            var defaultPostTime = ExternalConfig.PostTime;

            // Get mesage content from application properties
            var eventData = JsonConvert.DeserializeObject<Event>(JsonContent);

            if (MediaContent != null)
            {
                // Convert iFrame -> Jpeg image and store to photo container.
                var jpgBytes = H264Converter.GetInstance(FunctionDirectory)
                    .ConvertToImage(MediaContent, ImageType.JPEG);
                var photoBlob = PhotoBlobContainer.GetBlockBlobReference(
                    $"{DeviceId}/{eventData.Time.ToDatetimeString(TelemetryData.DateFormat)}.jpg");
                await photoBlob.UploadByteArray(jpgBytes);
                eventData.Photo = photoBlob.GetBlobPath();
            }

            // In case (shock or button event) and sdId is not kitting SDID (prefix is not<KITTING_SDID>)
            // -> create mdt document and insert into TelemetryData
            if (eventData.Flags.Shock == 1 || eventData.Flags.Button == 1)
            {
                if (SdId.StartsWith(ExternalConfig.KittingSdId))
                {
                    //Only upload mdt if sdId is not start with sdKitting prefix
                    Logger.Debug($"Shock or button but SdId is kitting: {SdId}. Stop process");
                }
                else
                {
                    // Get all entry for this deviceId from DeviceInfo table storage (filter data purpose)
                    var videoTimeItem = await DeviceInfoTable.RetrieveEntityAsync(new DeviceVideoTimeProperty(DeviceId, DeviceInfoSchema.VideoTime));
                    // Get pre & post time
                    var preTime = videoTimeItem?.Pre ?? defaultPreTime;
                    var postTime = videoTimeItem?.Post ?? defaultPostTime;

                    var startRequestTime = eventData.Time - preTime;
                    var endRequestTime = eventData.Time + postTime - 1;
                    Logger.Debug($"Got start: {startRequestTime} - End: {endRequestTime}");

                    var mdtArr = new List<string>();
                    // Create mdtRequest documents
                    for (var idx = startRequestTime; idx <= endRequestTime; idx++)
                    {
                        try
                        {
                            await DbClient.CreateTelemetryAsync(DeviceId, SdId, 0, 0, new Mdt { Time = idx });
                            mdtArr.Add(idx.ToString());
                        }
                        catch (DocumentClientException docEx)
                        {
                            //Only ignore conflict case. Mdt is already created in database --> Skip
                            Logger.Warning($"Record existed: {idx}");
                            if (docEx.StatusCode != HttpStatusCode.Conflict)
                            {
                                throw;
                            }
                        }
                    }

                    //Special cases: Tcu send duplicated shock event. Do not need to send same "eventData" and video
                    if (!mdtArr.Any())
                    {
                        Logger.Warning($"Duplicated emergency event at: {eventData.Time}");
                        return;
                    }

                    // Update DataRequest table storage
                    await DataRequestTable.UpsertEntityAsync(
                        new DataRequest(DeviceId, startRequestTime, endRequestTime, eventData.CreateId()));

                    var mdtRequest = string.Join(",", mdtArr.ToArray());

                    // - OnEvent message queue
                    var notificationMsg = new DeviceNotificationMessage
                    {
                        DeviceId = DeviceId,
                        SdId = SdId,
                        actionType = DeviceNotificationMessage.ActionType.OnMdtRequest,
                        Parameter = mdtRequest
                    };

                    await DeviceNotificationQueue.PushQueueMessageAsync(notificationMsg);
                }
            }

            await DbClient.UpsertTelemetryAsync(DeviceId, SdId, SentTime, ReceivedTime, eventData);

            // -Tramission event hub data
            var jsonData = new
            {
                deviceId = DeviceId,
                sdId = SdId,
                eventData.Time,
                eventData.Gps,
                eventData.Flags,
                eventData.Strength,
                eventData.Photo
            };
            var transmissionData = TransmissionData.Create(jsonData.ToJsonBytes(), DeviceId, MessageType.Event, SdId, eventData.Time);
            await TransmissionEventHub.PushEventDataAsync(transmissionData);
        }
    }
}