﻿using JB2.Core;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;
using Microsoft.ServiceBus.Messaging;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Table;
using System.Threading.Tasks;

namespace JB2.MainProc
{
    public class MessageCollection
    {
        static MessageCollection() { new VersionLogger(); }

        public static async Task Run(EventData eventData, TraceWriter writer, ILogger logger, ExecutionContext context,
            CloudTable deviceInfoInput,
            CloudTable dataRequestInput,
            CloudTable settingInfoInput,
            CloudTable latestInfoInput,
            CloudBlobContainer photoBlobContainer,
            CloudBlobContainer iothubBlobContainer,
            CloudBlobContainer sensorDataContainer,
            DocumentClient dbClient,
            IAsyncCollector<EventData> transmissionEventHub,
            IAsyncCollector<EventProcessingMessage> eventDataQueueItems,
            IAsyncCollector<DeviceNotificationMessage> deviceNotificationQueueItems,
            IAsyncCollector<EventData> outEventHubPoison)
        {
            var msg = new JB2Message(eventData);
            if (!msg.ParseIoTData(logger, iothubBlobContainer))
            {
                return;
            }
            logger = logger.AddDeviceIdProp(msg.DeviceId);

            BaseMessageProcessor messageProcessor = null;
            switch (msg.MessageType)
            {
                case MessageType.Startup:
                    messageProcessor = new StartupMessageProcessor();
                    break;
                case MessageType.Reset:
                    messageProcessor = new ResetMessageProcessor();
                    break;
                case MessageType.Event:
                    messageProcessor = new EventMessageProcessor();
                    break;
                case MessageType.Mdt:
                    messageProcessor = new MdtMessageProccessor();
                    break;
                case MessageType.Vpc:
                    messageProcessor = new VpcMessageProcessor();
                    break;
                case MessageType.Log:
                    messageProcessor = new LogMessageProcessor();
                    break;
                case MessageType.Log2:
                    messageProcessor = new Log2MessageProcessor();
                    break;
                case MessageType.Error:
                    messageProcessor = new ErrorMessageProcessor();
                    break;
                case MessageType.Setting:
                    logger.Warning($"Abolished message type <setting> is sent");
                    return;
                default:
                    logger.Warning($"Unhandle message type <{msg.MessageType}> is sent");
                    return;
            }

            messageProcessor.DbClient = dbClient;
            messageProcessor.DeviceInfoTable = deviceInfoInput;
            messageProcessor.SettingInfoTable = settingInfoInput;
            messageProcessor.DataRequestTable = dataRequestInput;
            messageProcessor.LatestInfoTable = latestInfoInput;
            messageProcessor.SensorContainer = sensorDataContainer;
            messageProcessor.PhotoBlobContainer = photoBlobContainer;
            messageProcessor.IoTHubBlobContainer = iothubBlobContainer;
            messageProcessor.DeviceNotificationQueue = deviceNotificationQueueItems;
            messageProcessor.EventDataQueueItems = eventDataQueueItems;
            messageProcessor.TransmissionEventHub = transmissionEventHub;
            messageProcessor.FunctionDirectory = context.FunctionDirectory;

            await messageProcessor.ExecuteAsync(msg, logger, outEventHubPoison);
        }
    }
}