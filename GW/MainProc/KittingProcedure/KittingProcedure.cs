﻿using JB2.Core;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using Microsoft.ServiceBus.Messaging;
using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json;
using System.Text;
using System.Threading.Tasks;

namespace JB2.MainProc
{
    public class KittingProcedure
    {
        protected KittingProcedure() { }
        public static async Task Run(EventData tranmissionData, ILogger logger, CloudBlobContainer kittingOutput,
            CloudBlobContainer photoInput, ExecutionContext context)
        {
            var msg = new JB2Message(tranmissionData);
            if (!msg.ParseData(logger))
            {
                return;
            }
            logger = logger.AddDeviceIdProp(msg.DeviceId);
            if (!msg.SdId.StartsWith(ExternalConfig.KittingSdId))
            {
                return;
            }
            logger.Info($"Kitting message: {msg.MessageType}");

            if (msg.MessageType == MessageType.Startup)
            {
                // If startup messageType, store the content of the message into the kitting BLOB
                var blob = kittingOutput.GetBlockBlobReference($"{msg.SdId}_{msg.DeviceId}.json");
                await blob.UploadByteArray(tranmissionData.GetBytes());
            }
            else if (msg.MessageType == MessageType.Event)
            {
                dynamic eventMessage = JsonConvert.DeserializeObject(Encoding.UTF8.GetString(tranmissionData.GetBytes()));

                // If not button event, return.
                if (eventMessage.flags.button != 1)
                {
                    return;
                }

                // If event messageType and button event, copy the img file to kitting BLOB
                var srcBlob = photoInput.GetBlockBlobReference(((string)eventMessage.photo).Replace("photo/", ""));
                if (!await srcBlob.ExistsAsync())
                {
                    logger.Warning($"Blob not found: {srcBlob.Name}");
                    return;
                }

                var destBlob = kittingOutput.GetBlockBlobReference($"{msg.SdId}_{msg.DeviceId}.jpg");
                await destBlob.UploadByteArray(await srcBlob.DownloadByteArrayAsync());
            }
        }
    }
}