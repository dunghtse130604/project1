﻿using JB2.Core;
using Microsoft.Azure.Devices;
using Microsoft.Azure.Devices.Common;
using Microsoft.Azure.Devices.Common.Exceptions;
using Microsoft.Azure.Devices.Shared;
using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace JB2.MainProc
{
    public class RegistrationApi
    {
        protected RegistrationApi() { }

        private static readonly string IoTConnectionString = AppEnvironment.IoTHubConnectionString;
        private static readonly Uri ConfigurationApiUrl = AppEnvironment.ConfigurationApiUrl;
        private static readonly Lazy<RegistryManager> lazyRegistryManager =
            new Lazy<RegistryManager>(() => RegistryManager.CreateFromConnectionString(IoTConnectionString));
        private static RegistryManager registryManager => lazyRegistryManager.Value;

        public const string DeviceIdParam = "deviceId";
        public const string DeviceKeyParam = "deviceKey";

        public static async Task<HttpResponseMessage> Run(HttpRequestMessage req, ILogger logger, CloudTable deviceInfoInput)
        {
            // 1. Parser request
            var content = await req.Content.ReadAsStringAsync();
            var keyValues = GetNameValuePair(content);

            string deviceIdReq = null;
            string deviceKeyReq = null;
            if (keyValues.ContainsKey(DeviceIdParam) &&
                !keyValues[DeviceIdParam].IsNullOrWhiteSpace())
            {
                deviceIdReq = HttpUtility.UrlDecode(keyValues[DeviceIdParam]);
            }

            if (keyValues.ContainsKey(DeviceKeyParam) &&
                !keyValues[DeviceKeyParam].IsNullOrWhiteSpace())
            {
                deviceKeyReq = HttpUtility.UrlDecode(keyValues[DeviceKeyParam]);
            }

            logger = logger.AddDeviceIdProp(deviceIdReq);
            logger.Info($"Regstration: deviceKey={deviceKeyReq?.Substring(0, Math.Min(deviceKeyReq.Length, 6))}");

            if (string.IsNullOrEmpty(deviceIdReq))
            {
                logger.Warning("Device id is empty");
                return new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("DeviceId cannot be empty!", Encoding.UTF8)
                };
            }

            // 2. Register deviceId to IoTHub. If deviceKey is set, set the deviceKey to IotHub. If not, use auto generation of IoTHub.
            var device = await RegisterDeviceAsync(deviceIdReq, deviceKeyReq, logger);
            var deviceConnectionString = CreateDeviceConnectionString(device);

            if (deviceConnectionString.IsNullOrWhiteSpace() && !deviceKeyReq.IsNullOrWhiteSpace())
            {
                logger.Warning("Device Key is wrong format. DeviceId must valid base-64 format with a key length between 16 and 64 bytes ");
                return new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("DeviceKey must be in valid base-64 format with a key length between 16 and 64 bytes", Encoding.UTF8)
                };
            }

            // 3.Set Device Api Url to DeviceTwin
            await SetUrlToDeviceTwin(deviceIdReq);

            // 4.Remove valid, lastSystemLogIndex and lastErrorLogIndex properties from DeviceInfo table
            string[] rowKeyList = { DeviceInfoSchema.Valid,
                                    DeviceInfoSchema.LastSystemLogIndex,
                                    DeviceInfoSchema.LastErrorLogIndex };
            var operations = new TableBatchOperation();
            deviceInfoInput.QueryDeviceEntities<TableEntity>(deviceIdReq)
                .Where(s => rowKeyList.Contains(s.RowKey))
                .ToList()
                .ForEach(item => operations.Delete(item));
            if (operations.Any())
            {
                await deviceInfoInput.ExecuteBatchAsync(operations);
            }

            // 5.Set lastReceivedDriveTime with current time to DeviceInfo table
            await deviceInfoInput.UpsertEntityAsync(
                new DeviceInfo<long>(deviceIdReq, DeviceInfoSchema.LastReceivedDriveTime, UnixTime.Now));

            // Set lastResetTime with current time to DeviceInfo table
            await deviceInfoInput.UpsertEntityAsync(
                new DeviceInfo<long>(deviceIdReq, DeviceInfoSchema.LastResetTime, UnixTime.Now));

            // 6.Response body : 
            return new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(deviceConnectionString, Encoding.UTF8)
            };
        }

        public static async Task<Device> RegisterDeviceAsync(string deviceId, string deviceKeyReq, ILogger logger)
        {
            var device = new Device(deviceId);
            try
            {
                //If deviceKey is set, register with the deviceKey
                if (!deviceKeyReq.IsNullOrWhiteSpace())
                {
                    device.Authentication = new AuthenticationMechanism
                    {
                        SymmetricKey =
                        {
                            PrimaryKey = deviceKeyReq,
                            SecondaryKey = deviceKeyReq
                        }
                    };
                }
                device = await registryManager.AddDeviceAsync(device);
            }
            catch (DeviceAlreadyExistsException)
            {
                logger.Info($"Device already registered");
                //If deviceKey is set, overwrite with the deviceKey, else, get the current device
                device = !deviceKeyReq.IsNullOrWhiteSpace()
                    ? await registryManager.UpdateDeviceAsync(device, true)
                    : await registryManager.GetDeviceAsync(deviceId);
            }
            catch (Exception ex)
            {
                logger.Exception(ex);
                return null;
            }

            return device;
        }

        // Referred a part of DeviceExplorer (https://github.com/Azure/azure-iot-sdk-csharp/blob/master/tools/DeviceExplorer/DeviceExplorer/DevicesProcessor.cs)
        private static string CreateDeviceConnectionString(Device device)
        {
            if (device == null) return string.Empty;
            StringBuilder deviceConnectionString = new StringBuilder();

            var hostName = string.Empty;
            var tokenArray = IoTConnectionString.Split(';');
            foreach (string token in tokenArray)
            {
                var keyValueArray = token.Split('=');
                if (keyValueArray[0] == "HostName")
                {
                    hostName = token + ';';
                    break;
                }
            }

            if (!String.IsNullOrWhiteSpace(hostName))
            {
                deviceConnectionString.Append(hostName);
                deviceConnectionString.AppendFormat("DeviceId={0}", device.Id);

                if (device.Authentication != null)
                {
                    if ((device.Authentication.SymmetricKey != null) &&
                        (device.Authentication.SymmetricKey.PrimaryKey != null))
                    {
                        deviceConnectionString.AppendFormat(";SharedAccessKey={0}",
                            device.Authentication.SymmetricKey.PrimaryKey);
                    }
                    else
                    {
                        deviceConnectionString.AppendFormat("{0}", ";x509=true");
                    }
                }
            }

            return deviceConnectionString.ToString();
        }

        public static async Task SetUrlToDeviceTwin(string deviceId)
        {
            var authenKey = Uri.EscapeDataString(SecurityUtils.GenerateAuthenKey(IoTConnectionString, deviceId));
            var url = $"{ConfigurationApiUrl}/{deviceId}?code={authenKey}";
            var deviceTwin = new Twin();
            deviceTwin.Properties.Desired["url"] = url;
            await registryManager.UpdateTwinAsync(deviceId, deviceTwin, "*");
        }

        private static IDictionary<string, string> GetNameValuePair(string textParameter)
        {
            var pairs = textParameter
                .Split('&')
                .Select(part => part.Split('='))
                .Where(part => part.Length == 2)
                .ToDictionary(param => param[0], param => param[1]);
            return pairs;
        }
    }
}