﻿using JB2.Core;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using Microsoft.ServiceBus.Messaging;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace JB2.DriveProc
{
    public static class DriveDataProcessing
    {
        private static readonly HttpClient _client = new HttpClient()
        {
            Timeout = TimeSpan.FromSeconds(20)
        };

        private static readonly GlobalMetricAggerator Metric = new GlobalMetricAggerator("DriveDataProc");

        public static async Task Run(DriveProcessingMessage message, ILogger logger, DocumentClient dbClient, CloudTable driveRowData, IAsyncCollector<EventData> driveEventHub)
        {
            var sw = Stopwatch.StartNew();
            try
            {
                await Process(message, logger, dbClient, driveRowData, driveEventHub);
            }
            finally
            {
                Metric.Add(sw);
            }
        }

        public static async Task Process(DriveProcessingMessage message, ILogger logger, DocumentClient dbClient, CloudTable driveRowData, IAsyncCollector<EventData> driveEventHub)
        {
            logger = logger.AddDeviceIdProp(message.DeviceId);
            var deviceId = message.DeviceId;
            var fromTime = message.Time;
            var toTime = message.Time - (fromTime % 60) + ExternalConfig.SendInterval;
            var engineOnTime = message.EngineOnTime;

            // Check if message is old from TCU
            if (fromTime < UnixTime.Now - (ExternalConfig.DayLimit * 86400))
            {
                logger.Info($"Drive data of {ExternalConfig.DayLimit} days-old: message={message}");
                return;
            }

            // Get DriveData record and calculate fromTime of package data
            var driveDataDocs = dbClient
                .TelemetryQuery<DriveData>(deviceId)
                .Where(s => s.MessageType == MessageType.DriveData && s.Data.Time >= fromTime && s.Data.Time < toTime)
                .AsEnumerable()
                .Where(s => s.Data.EngineOnTime == engineOnTime)
                .OrderBy(s => s.Data.Time)
                .AsEnumerable();
            if (driveDataDocs.Any())
            {
                var lastDriveData = driveDataDocs.Last();
                fromTime = lastDriveData.Data.Time + lastDriveData.Data.Duration;
            }

            // Get Drive document with 1 previous minute for DriveCalculator (actually needs data for previous 30 seconds)
            var driveTelemetryFromTime = (fromTime / 60 - 1) * 60;
            var driveDocs = dbClient
                .TelemetryQuery<Drive>(deviceId)
                .Where(s => s.MessageType == MessageType.Drive && s.Data.Time >= driveTelemetryFromTime && s.Data.Time < toTime)
                .AsEnumerable()
                .Where(s => s.Data.EngineOnTime == engineOnTime)
                .OrderBy(s => s.Data.Time)
                .AsEnumerable();
            if (!driveDocs.Any())
            {
                logger.Error($"Not found any records: message={message}");
                return;
            }

            var firstDriveDoc = driveDocs.First();
            var lastDriveDoc = driveDocs.Last();
            var driveDocStartTime = firstDriveDoc.Data.Time;
            var driveDocEndTime = lastDriveDoc.Data.Time + lastDriveDoc.Data.Duration;

            // message.time is calculated from engineOnTime in case of first pack frame of a trip.
            // Sometimes engineOnTime becomes earlier than the actual lead time of a trip (around 15 sec earlier),
            // so in this case, update fromTime with the lead time of the target data.
            if (fromTime < driveDocStartTime)
            {
                fromTime = driveDocStartTime;
            }

            // Check if message is processed before or not
            if (fromTime >= driveDocEndTime)
            {
                logger.Warning($"Duplicated process: message={message}");
                return;
            }

            // Check if data is continuous. Compare total duration of documents with endTime - startTime
            var driveDocsDuration = driveDocs.Sum(s => s.Data.Duration);
            if (driveDocsDuration < driveDocEndTime - driveDocStartTime)
            {
                logger.Warning($"Non-contiguous data: message={message}");
                return;
            }

            // Get CSV Data from DriveRowData TableStorage
            var driveDataList = new List<DriveCsvData>();
            foreach (var driveDocument in driveDocs)
            {
                var entity = await driveRowData.RetrieveEntityAsync(new DriveRowData(deviceId, driveDocument.Data.Time));
                if (entity?.Content == null)
                {
                    logger.Error($"Entity or content null: deviceId={deviceId}, time={driveDocument.Data.Time}");
                    continue;
                }
                driveDataList.AddRange(CsvUtils.ParseCsvString<DriveCsvData>(entity.Content));
            }
            // If total duration in document DB and CSV lines is not same, there is a error when process DocumentDB or StorageAccount
            if (driveDocsDuration != driveDataList.Count)
            {
                logger.Error(
                    $"Lacked data: Document duration={driveDocsDuration}, CSV duration={driveDataList.Count}, message={message}");
                return;
            }
            // Remove extra data older than 30 seconds before
            int ignoreCount = 0;
            foreach (var cells in driveDataList)
            {
                if (cells.Time >= fromTime - 30)
                {
                    break;
                }
                ignoreCount++;
            }
            driveDataList = driveDataList.Skip(ignoreCount).ToList();
            if (!driveDataList.Any())
            {
                logger.Warning($"No data to export: message={message}");
                return;
            }

            // Prepare data for calculating Est values
            var gpsValue = new List<double?[]>();
            foreach (var driveData in driveDataList)
            {
                gpsValue.Add(driveData.GpsValid == 1
                    ? new double?[] { driveData.GpsLatitude, driveData.GpsLongitude }
                    : new double?[] { null, null });
            }

            // Call DriveCalculator and get the result
            var response = await _client.PostAsJsonAsync(AppEnvironment.DriveCalculatorUrl, gpsValue);
            if (!response.IsSuccessStatusCode)
            {
                var detail = await response.Content.ReadAsStringAsync();
                if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    logger.Error($"DriveCalculator failed (408): {detail}, message={message}");
                    return;
                }
                throw new Exception($"DriveCalculator failed ({response.StatusCode}): {detail}");
            }
            var estData = await response.Content.ReadAsAsync<double?[][]>();

            // Create output csv data.
            var outputDriveDataList = new List<OutputDriveCsvData>();
            for (var i = 0; i < driveDataList.Count; i++)
            {
                var driveData = driveDataList[i];
                // Skip previous data from fromTime
                if (driveData.Time < fromTime)
                {
                    continue;
                }
                outputDriveDataList.Add(new OutputDriveCsvData(driveData, estData[i]));
            }

            var transmissionData = TransmissionData.Create(GzipUtils.Compress(CsvUtils.ConvertCsvBytes(outputDriveDataList)),
                deviceId, MessageType.Drive, firstDriveDoc.SdId, fromTime);
            await driveEventHub.PushEventDataAsync(transmissionData);

            var driveEntity = new DriveData
            {
                EngineOnTime = engineOnTime,
                Time = fromTime,
                Duration = outputDriveDataList.Count
            };
            await dbClient.UpsertTelemetryAsync(
                deviceId, firstDriveDoc.SdId, UnixTime.Now, UnixTime.Now, driveEntity, ExternalConfig.DocumentTimeToLive);

            logger.Info($"Generate driveData: time={fromTime}, duration={outputDriveDataList.Count}");
        }
    }
}