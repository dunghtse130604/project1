﻿using JB2.Core;
using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace JB2.API
{
    public class Blob
    {
        public static async Task<HttpResponseMessage> Run(HttpRequestMessage req, string blobName, string deviceId, CloudBlobDirectory blobDirectory, ILogger logger)
        {
            // set default to read permissions
            const SharedAccessBlobPermissions permissions = SharedAccessBlobPermissions.Read;
            var container = blobDirectory.Container;

            ////Authentication validate
            if (!await SecurityUtils.IsAuthenticatedAsync(req, logger))
            {
                return new HttpResponseMessage(HttpStatusCode.Unauthorized);
            }
            var blobPath = deviceId + "/" + blobName;
            var sasToken = BlobUtils.GetBlobSasToken(container, blobPath, permissions);

            var response = new HttpResponseMessage(HttpStatusCode.Redirect);
            response.Headers.Location = new Uri($"{blobDirectory.Uri}/{blobPath}{sasToken}");

            logger.Debug("Redirect location: " + response.Headers.Location);
            return response;
        }

        protected Blob() { }
    }
}