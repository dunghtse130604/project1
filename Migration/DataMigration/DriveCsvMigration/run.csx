#r "Microsoft.WindowsAzure.Storage"

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.WindowsAzure.Storage.Blob;

public class MigrateMessage
{
    public string deviceId { get; set; }
    public string date { get; set; }
}

public static async Task Run(MigrateMessage migrateMessage, CloudBlobContainer inDriveDataContainer,
   CloudBlobContainer outDriveDataContainer, TraceWriter log)
{
    var deviceId = migrateMessage.deviceId;
    var date = migrateMessage.date;
    var hours = 24;
    var dateFormat = "yyyy/MM/dd";

    if (date.Length > 10)
    {
        dateFormat += " HH";
        hours = 1;
    }

    //Input is JST time, convert it to UTC+00
    var utcDate = DateTimeOffset.ParseExact($"{date}+09:00", dateFormat + "zzz",
                                    CultureInfo.CurrentCulture, DateTimeStyles.None).ToUniversalTime();
    var pathList = new List<string>();
    for (var i = 0; i < hours; i++)
    {
        pathList.Add(utcDate.AddHours(i).ToString("yyyyMMdd_HH"));
    }

    var trips = pathList.SelectMany(path => inDriveDataContainer.ListBlobs($"{deviceId}/{path}"))
                        .OfType<CloudBlockBlob>()
                        .Select(b => b.DownloadText(Encoding.UTF8).Trim())
                        .GroupBy(content => content.Split(new char[] { '\n' }, 2)[1].Split(new char[] { '\t' }, 4)[2])
                        .ToList();
    if (!trips.Any()) return;
    foreach (var trip in trips)
    {
        var firstMinute = trip.First().Split('\n');
        var header = firstMinute.First() + "\n";
        var startTimeStr = firstMinute[1].Split('\t')[3];
        var lines = trip.SelectMany(b => b.Split('\n').Skip(1));
        var content = header + string.Join("\n", lines);
        var startTime = DateTimeOffset.FromUnixTimeSeconds(long.Parse(startTimeStr)).AddHours(9);
        await outDriveDataContainer
            .GetBlockBlobReference($"{startTime:yyyy/MM/dd}/{deviceId}/{startTime:HHmmss}.csv")
            .UploadTextAsync(content);
    }
}