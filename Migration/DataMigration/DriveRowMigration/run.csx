﻿#r "Microsoft.WindowsAzure.Storage"
#r "Microsoft.Azure.Documents.Client"

using Microsoft.Azure.Documents.Client;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class MigrateMessage
{
    public string deviceId { get; set; }
    public string date { get; set; }
}

public class DriveDocument
{
    public long _ts { get; set; }
    public string deviceId { get; set; }
    public string messageType { get; set; }
    public Data data { get; set; }
}

public class Data
{
    public string engineOnTime { get; set; }
    public long time { get; set; }
}

public class DriveRowData : TableEntity
{
    public const string TableDateFormat = "yyyyMMdd_HHmmss";

    public string content { get; set; }
}

public static async Task Run(MigrateMessage migrateMessage, DocumentClient dbClient, CloudTable driveRowData, CloudBlobContainer driveData, TraceWriter log)
{
    // Query drive documents created in the target date (by JST)
    // For avoiding drive data at end of day (23:59:00) to be separated due to different _ts dates, slide _ts range condition by particular margin period (5 minutes)
    // and query by _ts for the margin period before the above range for the data driven in the day.
    // To query the above conditions, make the following two queries and conbine the results.
    // - Condition 1: WHERE date        <= c._ts < date + 5min        AND date <= c.date.time < date + 1day
    // - Condition 2: WHERE date + 5min <= c._ts < date + 1day + 5min AND         c.date.time < date + 1day
    var date = DateTimeOffset.ParseExact($"{migrateMessage.date}+09:00", "yyyy/MM/ddzzz", CultureInfo.InvariantCulture, DateTimeStyles.None);
    var driveQuery = dbClient.CreateDocumentQuery<DriveDocument>(UriFactory.CreateDocumentCollectionUri("jb2-db", "TelemetryData"))
        .Where(d => d.deviceId == migrateMessage.deviceId && d.messageType == "drive");
    var dateBegin = date.ToUnixTimeSeconds();
    var dateEnd = date.AddDays(1).ToUnixTimeSeconds();
    var margin = 300;   // 5 minutes
    var driveDocs1 = driveQuery
        .Where(d => d._ts >= dateBegin && d._ts < dateBegin + margin && d.data.time >= dateBegin && d.data.time < dateEnd)
        .ToList();
    var driveDocs2 = driveQuery
        .Where(d => d._ts >= dateBegin + margin && d._ts < dateEnd + margin && d.data.time < dateEnd)
        .ToList();

    // Group by date and engineOnTime
    var trips = driveDocs1.Concat(driveDocs2)
        .OrderBy(d => d.data.time)
        .GroupBy(d => (d.data.time + 32400) / 86400)
        .SelectMany(g => g.GroupBy(d => d.data.engineOnTime))
        .ToList();

    foreach (var trip in trips)
    {
        var deviceId = trip.First().deviceId;
        var startTime = DateTimeOffset.FromUnixTimeSeconds(trip.First().data.time);
        var endTime = DateTimeOffset.FromUnixTimeSeconds(trip.Last().data.time);
        var entities = driveRowData.CreateQuery<DriveRowData>()
            .Where(e => e.PartitionKey == deviceId &&
                        e.RowKey.CompareTo(startTime.ToString(DriveRowData.TableDateFormat)) >= 0 &&
                        e.RowKey.CompareTo(endTime.ToString(DriveRowData.TableDateFormat)) <= 0)
            .ToList();
        if (trip.Count() != entities.Count)
        {
            log.Warning($"{deviceId}-{startTime}-{endTime}-{trip.Key}. Entities={entities.Count} does not equal documents={trip.Count()}");
        }
        var driveRowCsv = new StringBuilder();
        foreach (var entity in entities)
        {
            var headerLength = entity.content.IndexOf('\n') + 1;
            if (driveRowCsv.Length == 0)
            {
                // Add header line at first
                driveRowCsv.Append(entity.content.Substring(0, headerLength));
            }
            // Append csv content without header. AppendLine() and Trim() to support for both case: With empty line and no empty line.
            driveRowCsv.AppendLine(entity.content.Substring(headerLength).Trim());
        }
        if (driveRowCsv.Length > 0)
        {
            // Store to path composed of date, deviceId and start time of the trip (by JST)
            var datetime = startTime.ToOffset(TimeSpan.FromHours(9));
            var blockBlob = driveData.GetBlockBlobReference($"{datetime:yyyy/MM/dd}/{deviceId}/{datetime:HHmmss}.csv");
            await blockBlob.UploadTextAsync(driveRowCsv.ToString().Trim());
        }
    }
}
