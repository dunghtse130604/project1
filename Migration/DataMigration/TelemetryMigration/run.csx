#r "Microsoft.WindowsAzure.Storage"
#r "Microsoft.Azure.Documents.Client"
#r "Newtonsoft.Json"
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.RetryPolicies;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class MigrateMessage
{
    public string deviceId { get; set; }
    public string date { get; set; }
}

public static async Task Run(MigrateMessage migrateMessage, DocumentClient dbClient, CloudBlobContainer telemetryContainer, TraceWriter log)
{
    var deviceId = migrateMessage.deviceId;
    var date = migrateMessage.date;
    var dateFormat = "yyyy/MM/dd";
    var hours = 24;
    if (date.Length > 10)
    {
        dateFormat += " HH";
        hours = 1;
    }
    var startTime = DateTimeOffset.ParseExact($"{date}+09:00", dateFormat + "zzz", CultureInfo.CurrentCulture, DateTimeStyles.None)
        .ToUnixTimeSeconds();
    var endTime = startTime + TimeSpan.FromHours(hours).TotalSeconds;
    var documents = dbClient.CreateDocumentQuery<Document>(
            UriFactory.CreateDocumentCollectionUri("jb2-db", "TelemetryData"),
            "SELECT c.deviceId, c.sdId, c.sentTime, c.receivedTime, c.messageType, c.id, c.data FROM c" +
            $" WHERE c._ts >= {startTime} and c._ts < {endTime} ORDER BY c._ts ASC",
            new FeedOptions { PartitionKey = new PartitionKey(deviceId) })
        .ToList().Select(d => Encoding.UTF8.GetString(d.ToByteArray()));

    if (documents.Any())
    {
        var blob = string.Join("\n", documents);
        var datePath = date.Replace(' ', '/');
        await telemetryContainer.GetBlockBlobReference($"{datePath}/{deviceId}.blob").UploadTextAsync(blob);
    }
}