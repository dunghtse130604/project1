﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace JB2.BOM
{
    [Table("QueryCache")]
    public partial class QueryCache
    {
        [Key]
        [StringLength(500)]
        public string query { get; set; }
        public int status { get; set; }
        public DateTime updatedDate { get; set; }
        public int dataCount { get; set; }

        public const string TableName = "QueryCache";
        public static string CommandCreateTable = $@"
            DROP TABLE IF EXISTS {TableName};
            CREATE TABLE {TableName} (
                query varchar(500) NOT NULL,
                status int NOT NULL,
                updatedDate datetime NOT NULL,
                dataCount int NOT NULL DEFAULT '0'
            );";
    }
}