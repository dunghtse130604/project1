﻿using Microsoft.ApplicationInsights;
using Microsoft.ApplicationInsights.DataContracts;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace JB2.BOM
{
    public class HomeController : Controller
    {
        protected const string SignInPageUrl = "/Account/SignIn";
        protected const string DefaultPageUrl = "/device-report";

        public ActionResult Default()
        {
            return Redirect(DefaultPageUrl);
        }

        [HttpGet]
        public async Task<string> GetUsageInfo()
        {
            var usageInfo = await BomCore.GetUsageInfo();
            return $"Account {BomEnvironment.EmbedUsername}. UsageInfo={usageInfo}";
        }
    }
}