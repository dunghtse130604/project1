﻿using System;
using System.Globalization;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace JB2.BOM
{
    public class TelemetryReportController : HomeController
    {              
        [HttpGet]
        public async Task<ActionResult> Index(string deviceId)
        {
            if (!Request.IsAuthenticated)
            {
                return Redirect(SignInPageUrl);
            }

            if (System.Web.HttpContext.Current.Request.Url.Query == "")
            {
                ViewBag.deviceId = string.Empty;                
                return View(TokenModel.Ok());
            }
            if (String.IsNullOrEmpty(deviceId))
            {
                return View(TokenModel.Error(Resources.Global.th_notification_bad_request));
            }

            ViewBag.deviceId = deviceId;
            // Over 1 year _ts cost RU so much, so we need to reduce it by limit the time range query less than MonthLimit variable
            var fromTime = DateTimeOffset.UtcNow.AddMonths(-BomEnvironment.MonthLimit).ToUnixTimeSeconds();
            string queryString = $@"SELECT * FROM c WHERE c._ts >= {fromTime} AND c.messageType in ('startup', 'reset', 'event', 'error', 'eventData', 'vpc', 'log')";
            var count = await BomCore.MigrateData(deviceId, queryString);
            if (count > 0)
            {
                return View(await BomCore.GetToken(BomEnvironment.EmbedReportIdTelemetry));
            }
            if (count == 0)
            {
                return View(TokenModel.Error(Resources.Global.th_notification_no_data));
            }
            return  View(TokenModel.Error(Resources.Global.server_busy));
        }
    }
}