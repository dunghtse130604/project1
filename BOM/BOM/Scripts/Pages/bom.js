﻿$(document).ready(function () {
    var powerbi;
    // Reload PowerBI report
    $('#refresh').on('click',
        function () {
            let report = powerbi.get(reportContainer);
            report.reload();
        });

    // Automatically trip spaces for deviceId input
    $('#deviceId').focusout(function () {
        $('#deviceId').val($.trim($('#deviceId').val()));
    });

    // Automatically format date input
    $('#date').datetimepicker({
        format: "YYYY-MM-DD",
        maxDate: moment().endOf('d')
    });

    // Validate input in submiting
    var msgInvalidInput;
    $('#inputForm').validate({
        submitHandler: function () {
            let value = $.trim($('#deviceId').val());
            if (value.indexOf(' ') >= 0 || value === '') {
                $('#errorMessage').text(msgInvalidInput);
                return false;
            }

            $('#btnSubmit').prop('disabled', true);
            return true;
        }
    });
});

// Create Power BI Embeded report
let accessToken;
if (accessToken) {
    let models = window['powerbi-client'].models;

    let filters = [];
    if (typeof filterDeviceId === "string") {
        filters.push({
            $schema: "http://powerbi.com/product/schema#basic",
            target: {
                table: "Telemetry",
                column: "deviceId"
            },
            operator: "In",
            values: [filterDeviceId]
        });
    }
    if (typeof filterDate === "string") {
        filters.push({
            $schema: "http://powerbi.com/product/schema#advanced",
            target: {
                table: "Telemetry",
                column: "time"
            },
            logicalOperator: "And",
            conditions: [
                {
                    operator: "GreaterThanOrEqual",
                    value: filterDate + " 00:00:00"
                },
                {
                    operator: "LessThanOrEqual",
                    value: filterDate + " 23:59:59"
                }
            ]
        });
    }
    let embedUrl, reportId;
    let config = {
        type: 'report',
        tokenType: models.TokenType.Embed,
        accessToken: accessToken,
        embedUrl: embedUrl,
        permissions: models.Permissions.All,
        id: reportId,
        settings: {
            filterPaneEnabled: false,
            navContentPaneEnabled: false
        },
        filters: filters
    };
    if (typeof enablePageNavi === "boolean") {
        config.settings.navContentPaneEnabled = enablePageNavi;
    }

    let reportContainer = $('#reportContainer')[0];

    powerbi.embedNew(reportContainer, config);
}
