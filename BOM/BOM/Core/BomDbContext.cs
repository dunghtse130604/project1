﻿using Microsoft.Azure.Documents;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace JB2.BOM
{
    public class BomDbContext : DbContext
    {

        public BomDbContext(string connectionString) : base(connectionString)
        {

        }

        public virtual DbSet<QueryCache> QueryCaches { get; set; }


        public async Task CreateTables()
        {
            var connection = Database.Connection as SqlConnection;
            connection.Open();
            using (var cmd = new SqlCommand("", connection))
            {
                cmd.CommandText = TelemetryCache.CommandCreateTable;
                await cmd.ExecuteNonQueryAsync();
                cmd.CommandText = QueryCache.CommandCreateTable;
                await cmd.ExecuteNonQueryAsync();
            }
            connection.Close();
        }

        public async Task Insert(List<Document> documents)
        {
            var connection = Database.Connection as SqlConnection;
            connection.Open();
            using (var cmd = new SqlCommand("", connection))
            {
                //Create local temporary table #TelemetryCache with TelemetryCache structure
                //After connection close the table will automatic drop
                cmd.CommandText = TelemetryCache.CommandCreateTempTable;
                cmd.CommandType = CommandType.Text;
                await cmd.ExecuteNonQueryAsync();

                //Create structure of TelemetryCache table
                var dt = new DataTable();
                var adapter = new SqlDataAdapter(TelemetryCache.CommandCreateAdapter, connection);
                adapter.Fill(dt);

                //BulkCopy all data to #TelemetryCache
                foreach (var document in documents)
                {
                    try
                    {
                        var row = dt.NewRow();
                        MigrateTelemetryFromDoc(row, document);
                        dt.Rows.Add(row);
                    }
                    catch (DocumentClientException)
                    {
                        //Skip wrong format document. For example: Document does not have receivedTime
                        //Access to non existed property of document will make system throw DocumentClientException
                    }
                }
                var bulkCopy = new SqlBulkCopy(connection) { DestinationTableName = TelemetryCache.TempTableName };
                await bulkCopy.WriteToServerAsync(dt);

                //Merge temp talbe to TelemetryCache table
                cmd.CommandText = TelemetryCache.CommandMerge;
                cmd.CommandType = CommandType.Text;
                await cmd.ExecuteNonQueryAsync();
            }
            connection.Close();
        }

        private static void MigrateTelemetryFromDoc(DataRow dr, Document document)
        {
            // Cache of parent nodes for reducing reflection
            var docParser = new DocumentParser(document);

            foreach (var field in TelemetryCache.MigratedFields)
            {
                var value = docParser.FindValue(field.DocNode);
                if (value == null) {
                    continue;
                }

                if (field.Type.StartsWith("varchar"))
                {
                    int charLength = int.Parse(Regex.Replace(field.Type, @"[^0-9]", ""));
                    var str = (string)value;
                    if (!string.IsNullOrEmpty(str) && str.Length > charLength)
                    {
                        str = str.Substring(0, charLength);
                    }
                    dr[field.Name] = str;
                }
                else if (field.Type == "int")
                {
                    dr[field.Name] = value;
                }
                else if (field.Type == "float")
                {
                    dr[field.Name] = value;
                }
                else if (field.Type == "datetime")
                {
                    dr[field.Name] = DateTimeOffset.FromUnixTimeSeconds((long)value).AddHours(9).UtcDateTime;
                }
                else if (field.Type == "bit")
                {
                    dr[field.Name] = ((long)value == 1);
                }
            }

            // Custom parsing for DateId
            dr["dateId"] = DateTimeOffset.FromUnixTimeSeconds((long)docParser.FindValue("data.time"))
                .AddHours(9).ToString("yyyy-MM-dd");

            // Workaround: receivedTime of eventData is incorrect now, so use document updated timestamp instead.
            if ((string)docParser.FindValue("messageType") == "eventData")
            {
                dr["receivedTime"] = document.Timestamp.AddHours(9);
            }
        }

        private class DocumentParser
        {
            private Dictionary<string, Document> cache = new Dictionary<string, Document>();
            private Document document;

            public DocumentParser(Document doc)
            {
                document = doc;
            }

            // Find node value by format like "data.gps.time", with caching
            public object FindValue(string docNode)
            {
                var nodeNames = docNode.Split('.');
                Document node = document;

                // Explore the deepest inner node
                for (int i = 0; node != null && i < nodeNames.Length - 1; i++)
                {
                    var nodePath = string.Join(".", nodeNames.Take(i + 1).ToArray());
                    Document node2;
                    if (!cache.TryGetValue(nodePath, out node2))
                    {
                        // If the node is not cached yet, get the node from the document and store cache
                        node2 = node.GetPropertyValue<Document>(nodeNames[i]);
                        cache[nodePath] = node2;
                    }
                    node = node2;
                }
                return node?.GetPropertyValue<object>(nodeNames.Last());
            }

        }
    }
}