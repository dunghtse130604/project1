﻿using System.Configuration;
using System.IO;
using System;
using Microsoft.ApplicationInsights;
using Microsoft.ApplicationInsights.DataContracts;

namespace JB2.BOM
{
    public class BomEnvironment
    {
        public static string EmbedUsername;
        public static string EmbedPassword;
        public static string EmbedGroupId;
        public static string EmbedClientId;
        public static string EmbedReportIdDevice;
        public static string EmbedReportIdTelemetry;
        public static string EmbedReportIdTrip;

        public static string Tenant;
        public static string RedirectUrl;

        public static string DocumentDbConnStr;
        public static string SqlServerConnsStr;
        public static string VersionPage;

        public static int MonthLimit;

        // Do not add value for this constant.
        // Version string will be set by CICD not by manually. <tag> <commit> <date> <time> <author>
        public static string VersionString = "";

        public static void Initialize()
        {
            EmbedUsername = ConfigurationManager.AppSettings["EmbedUsername"];
            EmbedPassword = ConfigurationManager.AppSettings["EmbedPassword"];
            EmbedGroupId = ConfigurationManager.AppSettings["EmbedGroupId"];
            EmbedClientId = ConfigurationManager.AppSettings["EmbedClientId"];
            EmbedReportIdDevice = ConfigurationManager.AppSettings["EmbedReportIdDevice"];
            EmbedReportIdTelemetry = ConfigurationManager.AppSettings["EmbedReportIdTelemetry"];
            EmbedReportIdTrip = ConfigurationManager.AppSettings["EmbedReportIdTrip"];

            Tenant = ConfigurationManager.AppSettings["Tenant"];
            RedirectUrl = ConfigurationManager.AppSettings["RedirectUrl"];

            DocumentDbConnStr = ConfigurationManager.AppSettings["DocumentDbConnStr"];
            SqlServerConnsStr = ConfigurationManager.AppSettings["SqlConnStr"];
            MonthLimit = int.Parse(ConfigurationManager.AppSettings["MonthLimit"]);
#if DEBUG
            VersionPage = "Debugging";
#else
            VersionPage = VersionString.Split(' ')[0];
#endif
            //Log a verion string to application insights
            new TelemetryClient().TrackTrace($"Starting BOM ({VersionString})", SeverityLevel.Warning);
        }
    }
}
