﻿using System;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace JB2.BOM.Repository
{
    public class MultiLanguageActivator : IControllerActivator
    {
        private string defaultCulture = "ja-JP";
        public IController Create(RequestContext requestContext, Type controllerType)
        {
            //Available to switch culture in the future
            //if (requestContext.HttpContext.Session["Culture"] != null)
            //{
            //    defaultCulture = requestContext.HttpContext.Session["Culture"].ToString();
            //}

            Thread.CurrentThread.CurrentCulture = new CultureInfo(defaultCulture);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(defaultCulture);
            return DependencyResolver.Current.GetService(controllerType) as IController;
        }
    }
}