﻿using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.ApplicationInsights;


namespace JB2.BOM
{
    public static class DocumentDBRepo
    {
        private static readonly Uri CollectionUri = UriFactory.CreateDocumentCollectionUri("jb2-db", "TelemetryData");

        public static async Task<List<Document>> GetItemsAsync(string deviceId, string queryString, TelemetryClient ai)
        {
            var ru = 0.0;
            var numberOfDocs = 0;
            try
            {
                var builder = new DbConnectionStringBuilder { ConnectionString = BomEnvironment.DocumentDbConnStr };
                var authKey = builder["AccountKey"].ToString();
                var serviceEndpoint = builder["AccountEndpoint"].ToString();
                var client = new DocumentClient(new Uri(serviceEndpoint), authKey, new ConnectionPolicy
                {
                    ConnectionMode = ConnectionMode.Direct,
                    ConnectionProtocol = Protocol.Tcp,
                    MaxConnectionLimit = 100
                });

                var feedOptions = new FeedOptions
                {
                    MaxItemCount = -1,
                    PartitionKey = new PartitionKey(deviceId)
                };
                var result = new List<Document>();
                var query = client.CreateDocumentQuery<Document>(CollectionUri, queryString, feedOptions).AsDocumentQuery();
                while (query.HasMoreResults)
                {
                    var feed = await query.ExecuteNextAsync<Document>();
                    result.AddRange(feed);
                    ru += feed.RequestCharge;
                    numberOfDocs += feed.Count;
                }
                return result;
            }
            finally
            {
                ai.Info($"Consumption RU = {ru}. Number of documents = {numberOfDocs}");
            }

        }
    }
}