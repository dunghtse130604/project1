﻿using JB2.BOM.Repository;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace JB2.BOM
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            ControllerBuilder.Current.SetControllerFactory(new DefaultControllerFactory(new MultiLanguageActivator()));
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BomEnvironment.Initialize();
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            using (var context = new BomDbContext(BomEnvironment.SqlServerConnsStr))
            {
                context.CreateTables().Wait();
            }
        }
    }
}
