﻿using System.Web.Optimization;

namespace JB2.BOM
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Style/css").Include(
                "~/Content/Css/bootstrap/animate.min.css",
                "~/Content/Css/bootstrap/bootstrap.min.css",
                "~/Content/Css/bootstrap/tempusdominus-bootstrap-4.min.css",
                "~/Content/Css/Fontawesome/Awesome-css/font-awesome.min.css",
                "~/Content/Css/bom.css"
                ));         

            bundles.Add(new ScriptBundle("~/bundles/Libs").Include(
                "~/Scripts/Libs/moment/moment.js",
                "~/Scripts/Libs/moment/moment-timezone-with-data.js",
                "~/Scripts/Libs/bootstrap/bootstrap.min.js",
                "~/Scripts/Libs/bootstrap/tempusdominus-bootstrap-4.min.js",
                "~/Scripts/Libs/jquery/jquery-3.3.1.min.js",
                "~/Scripts/Libs/jquery/jquery.validate.min.js",
                "~/Scripts/Libs/powerBI/powerbi.min.js",
                "~/Scripts/Libs/powerBI/es6-promise.min.js"
            ));            

            bundles.Add(new ScriptBundle("~/bundles/Pages").Include(
                "~/Scripts/Pages/bom.js"));
        }
    }
}
