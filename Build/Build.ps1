Param(
    [parameter(Mandatory = $true, HelpMessage = "Please enter the path of the repository for building.")]
    [ValidateScript( {Test-Path $_ })]
    [String] $workSpace,

    [parameter(Mandatory = $false)]
    [String] $imageArea = "C:\JB2\ImageBuild",

    [parameter(Mandatory = $false)]
    [String] $version,

    [parameter(Mandatory = $false)]
    [string] $buildAuthor
)

Write-Host "--------------------------------"
Write-Host "START BUILD SCRIPT"
Write-Host "Workspace: $workSpace"
Write-Host "Image Area: $imageArea"
Write-Host "The version of building: $version"
Write-Host "Build Author: $buildAuthor"
Write-Host "--------------------------------"

#Validate required component. If failed, return immediately.
#Required component: MsBuild, Maven 
# - MsBuild component.
$msBuildPath = "C:\Program Files (x86)\MSBuild\14.0\Bin\MSBuild.exe"
if (Test-Path $msBuildPath) {
    Write-Output "[v] MsBuild Path : $msBuildPath"
}
else {
    Write-Warning "[x] Please install MsBuild and try again."
    exit
}

# - Maven component.
$mavenVersionCmd = "mvn --version"
try {
    Invoke-Expression $mavenVersionCmd
    Write-Output "[v] Maven is installed."
}
catch {
    Write-Warning "[x] Please install Maven and try again."
    exit
}

# - Get Path of Build Components.
$currentPath = (Split-Path -Parent $MyInvocation.MyCommand.Path)

# - Function for copy artifact from build source to image area.
function CopyArtifact($sourcePath, $isParent, $destination, $regexPath, $isExclude) {
    if ($isParent) {
        $imagePath = (Get-ChildItem -Path $sourcePath -Depth 0 -Directory -Recurse -Force).FullName
    }
    else {
        $imagePath = $sourcePath
    }
    [regex] $matchReg = (((Get-Content "$regexPath") | ForEach-Object {[regex]::escape($_).Replace("\$", "$")}) -join "|")
    if ($isExclude) {
        foreach ($itemPath in $imagePath) {
            Get-ChildItem -Path $itemPath -Recurse | Where-Object { $_.FullName -notmatch $matchReg} | Copy-Item -Destination {
                Join-Path $destination $_.FullName.Substring((Split-Path $itemPath -Parent).length)
            } -Force
        }
    }
    else {
        foreach ($itemPath in $imagePath) {
            Get-ChildItem -Path $itemPath -Recurse | Where-Object { $_.FullName -match $matchReg} | Copy-Item -Destination {
                Join-Path $destination $_.FullName.Substring((Split-Path $itemPath -Parent).length)
            } -Force
        }
    }
}

# - Path of build folder
$gwPath = Join-Path -Path $workSpace -ChildPath "GW"
$monitoringPath = Join-Path -Path $workSpace -ChildPath "Monitoring"
$bomPath = Join-Path -Path $workSpace -ChildPath "BOM\BOM"
$driveCalPath = Join-Path -Path $workSpace -ChildPath "Java\JavaCalc"
$recoveryPath = Join-Path -Path $workSpace -ChildPath "Failover\RecoveryProc"

# - Clean file in repository.
Set-Location "$workSpace"
Write-Output "Clean git"
git clean -d -x -f
git reset --hard

#Step1: Get Version Information
Write-Output "Get Version Information"
$revisionHash = git log -n 1 --pretty='%h'
if ([string]::IsNullOrWhitespace($buildAuthor)) {
    $author = git config --global user.name
}
else {
    $author = $buildAuthor
}
$date = Get-Date -format "yyyy-MM-dd hh:mm:sszzz"
$versionInfo = @"
$version $revisionHash $date $author
"@

#Remove "BOM-" prefix out of tag
$versionBOM = $versionInfo.replace('BOM-','')
$bomVersionPath = Join-Path -Path $workSpace -ChildPath "BOM\BOM\Core\BomEnvironment.cs"
(Get-Content $bomVersionPath).replace('VersionString = ""','VersionString = "'+ $versionBOM + '"') | Out-File $bomVersionPath -Encoding utf8

#Step2: Build Gateway
$nugetPath = Join-Path -Path $currentPath -ChildPath "BuildTools\nuget.exe"
$slnConfigPath = Join-Path -Path $workSpace -ChildPath "JB2.GatewayServer.sln"
$gwVersionPath = Join-Path -Path $workSpace -ChildPath "Common\Core\Configurations\VersionInfo.cs"
# change version info of GW
(Get-Content $gwVersionPath).replace('VersionString = ""','VersionString = "'+ $versionInfo + '"') | Out-File $gwVersionPath -Encoding utf8
# install Nuget package in build project
Write-Output "Install nuget package"
& $nugetPath restore $slnConfigPath
# Rebuild project
Write-Output "Please wait while build GW project"
$msBuild = & $msBuildPath "$workSpace\JB2.GatewayServer.sln" /t:Clean /t:Rebuild /property:Configuration=Release /property:Platform="Any CPU" /p:TargetFramework=v4.6.2
$msBuildResult = $msBuild | Select-String -Pattern "Build succeeded"
# check build status and change VERSION.txt file of GW
if ($msBuildResult) {
    Write-Output $msBuild
    Write-Output ("[v] Build GW completed successfully")
}
else {
    Write-Output $msBuild
    Write-Warning ("[x] Build GW failed ")
    exit $LastExitCode
}

#Step3: Build Drive Calculator
$calcFeatures = Join-Path -Path $driveCalPath -ChildPath "src\main\resources\calcFeatures.jar"
$calcVersionPath = Join-Path -Path $driveCalPath -ChildPath "src\main\java\jb2\drive\DriveCalculatorServlet.java"
# change version info of Drive Calculator
(Get-Content $calcVersionPath).replace('DRIVE_CALCULATOR_VERSION = ""','DRIVE_CALCULATOR_VERSION = "'+ $versionInfo + '"') | Out-File $calcVersionPath -Encoding ascii
# install calcFeatures.jar
mvn install:install-file -Dfile="$calcFeatures" -DgroupId="main.naviappli.calcfeatures" -DartifactId="calcFeatures" -Dversion="0.0.1" -Dpackaging=jar
# build DriveProcessing
Write-Output "Please wait while build Drive Calculator"
Set-Location "$driveCalPath"
$mvnDriveCalBuild = mvn clean package
$mvnDriveCalResult = $mvnDriveCalBuild | Select-String -Pattern "BUILD SUCCESS"
if ($mvnDriveCalResult) {
    Write-Output $mvnDriveCalBuild
    Write-Output "[v] Build Drive Calculator succeeded"
}
else {
    Write-Output $mvnDriveCalBuild
    Write-Warning "[x] Build Drive Calculator failed "
    exit $LastExitCode
}

#Step4: Copy new image to built image area
#Check imageArea Path
if (Test-Path $imageArea) {
    Get-ChildItem $imageArea -Recurse | Remove-Item -Recurse
}
else {
    New-Item -ItemType Directory -Force -Path $imageArea
}

#Copy deploy script
# create Script folder
$deployScriptArea = Join-Path -Path $imageArea -ChildPath "Script"
New-Item -ItemType Directory -Force -Path $deployScriptArea
# copy deploy script to imageArea
$versionInfo | Out-File "$deployScriptArea\VERSION.txt"
Copy-Item "$currentPath\Deploy.ps1" -Destination $deployScriptArea -Force -Recurse

#Copy Java artifact of Drive Calculator to imageArea
# create web app folder for Drive Calculator
$driveCalArea = Join-Path -Path $imageArea -ChildPath "JavaCalc\webapps"
New-Item -ItemType Directory -Force -Path $driveCalArea
# copy Java artifact to imageArea
Copy-Item "$driveCalPath\target\ROOT.war" -Destination $driveCalArea -Force -Recurse

#Get include and exclude file
$includeBOM = Join-Path -Path $currentPath -ChildPath "BuildTools\includefile_bom.txt"
$excludeGW = Join-Path -Path $currentPath -ChildPath "BuildTools\excludefile.txt"

#Copy .NET artifact GW to imageArea
CopyArtifact $gwPath $true $imageArea $excludeGW $true

#Copy .NET artifact Monitoring to imageArea
CopyArtifact $monitoringPath $true $imageArea $excludeGW $true

#Copy .NET artifact BOM webapp to imageArea
CopyArtifact $bomPath $false $imageArea $includeBOM $false

CopyArtifact $recoveryPath $false $imageArea $excludeGW $true
Write-Host "END BUILD SCRIPT"
Write-Host "--------------------------------"

Write-Output "LastExitCode = $LastExitCode"
exit $LastExitCode